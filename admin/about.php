<?php
/* MigToPG
 * Copyright (C) 2012		Florian Henry			<florian.henry@open-concept.pro>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 	\file		admin/about.php
 * 	\ingroup	migtopg
 * 	\brief		This file is about page
 */

error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('html_errors', false);

$res=@include("../../main.inc.php");				// For root directory
if (! $res) $res=@include("../../../main.inc.php");	// For "custom" directory

// Libraries
require_once DOL_DOCUMENT_ROOT . "/core/lib/admin.lib.php";
dol_include_once('/migtopg/lib/migtopg.lib.php');
dol_include_once('/migtopg/lib/PHP Markdown 1.0.1o/markdown.php');

// Translations
$langs->load("migtopg@migtopg");

// Access control
if ( ! $user->admin) accessforbidden();

// Parameters
$action = GETPOST('action', 'alpha');

/*
 * Actions
 */

/*
 * View
 */
$page_name = "MigToPGAbout";
llxHeader('', $langs->trans($page_name));

// Subheader
$linkback = '<a href="' . DOL_URL_ROOT . '/admin/modules.php">'
	. $langs->trans("BackToModuleList") . '</a>';
print_fiche_titre($langs->trans($page_name), $linkback);

// Configuration header
$head = migtopg_admin_prepare_head();
dol_fiche_head($head, 'about', $langs->trans("Module104000Name"), 0,"migtopg@migtopg");

// About page goes here
echo $langs->trans("MigToPGAboutPage");

$buffer = file_get_contents(dol_buildpath('/migtopg/README.md',0));
print Markdown($buffer);

print '<BR>';

print '<img src="'.dol_buildpath('/migtopg/img/gplv3.png',1).'"/>';

print '<a href="'.dol_buildpath('/migtopg/COPYING',1).'">License GPL V 3</a>';


llxFooter();

$db->close();
?>