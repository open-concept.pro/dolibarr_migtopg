<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
 *  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
 *  \file       htdocs/custom/migtopg/admin/adm_migtopg.php
 *  \ingroup    MigToPG
 *  \brief      Administration screen to prepare and test migratino to PostgreSQL database
 */

$res=@include("../../main.inc.php");				// For root directory
if (! $res) $res=@include("../../../main.inc.php");	// For "custom" directory

dol_include_once('/core/lib/admin.lib.php');
dol_include_once('/migtopg/class/html.formmigtopg.class.php');
dol_include_once('/migtopg/lib/migtopg.lib.php');

$langs->load("admin");
$langs->load("install");
$langs->load('migtopg@migtopg');

if (!$user->rights->migtopg->readconf) accessforbidden();

$action = GETPOST('action','alpha');

if ($action == 'update' && empty($_POST["cancel"]))
{
	$pg_server=GETPOST('MIG_TO_PG_DB_PG_SERVER','alpha');
	$res = dolibarr_set_const($db, 'MIG_TO_PG_DB_PG_SERVER', $pg_server,'chaine',0,'',$conf->entity);
	
	if (! $res > 0) $error++;
	
	$pg_server_port=GETPOST('MIG_TO_PG_DB_PG_PORT','int');
	if (is_numeric($pg_server_port) || $pg_server_port=='') {
		$res = dolibarr_set_const($db, 'MIG_TO_PG_DB_PG_PORT', $pg_server_port,'int',0,'',$conf->entity);
		if (! $res > 0) $error++;
	}
	else {
		$error++;
		$msg=$langs->trans("MigToPGERRPortInt");
	}
	
	$pg_server_name=GETPOST('MIG_TO_PG_DB_NAME','alpha');
	$res = dolibarr_set_const($db, 'MIG_TO_PG_DB_NAME', $pg_server_name,'chaine',0,'',$conf->entity);
	if (! $res > 0) $error++;
	
	$pg_server_user=GETPOST('MIG_TO_PG_DB_USER','alpha');
	$res = dolibarr_set_const($db, 'MIG_TO_PG_DB_USER', $pg_server_user,'chaine',0,'',$conf->entity);
	if (! $res > 0) $error++;
	
	$pg_server_pass=GETPOST('MIG_TO_PG_DB_PASS','alpha');
	$res = dolibarr_set_const($db, 'MIG_TO_PG_DB_PASS', $pg_server_pass,'chaine',0,'',$conf->entity);
	if (! $res > 0) $error++;
	
	$pg_server_root_user=GETPOST('MIG_TO_PG_DB_RUSER','alpha');
	$res = dolibarr_set_const($db, 'MIG_TO_PG_DB_RUSER', $pg_server_root_user,'chaine',0,'',$conf->entity);
	if (! $res > 0) $error++;
	
	$pg_server_root_pass=GETPOST('MIG_TO_PG_DB_RPASS','alpha');
	$res = dolibarr_set_const($db, 'MIG_TO_PG_DB_RPASS', $pg_server_root_pass,'chaine',0,'',$conf->entity);
	if (! $res > 0) $error++;

	if (! $res > 0) $error++;

	if (! $error)
	{
		$mesg = "<font class=\"ok\">".$langs->trans("SetupSaved")."</font>";
		$action='view';
	}
	else
	{
		$mesg = "<font class=\"error\">".$langs->trans("Error")." ".$msg."</font>";
		$action='edit';
	}
}

elseif (empty($action) ||  (!empty($_POST["cancel"]))) {
	$action='view';
}

/*
 *  Admin Form
*
*/

llxHeader();

$form=new Form($db);
$formMigToPG=new FormMigToPG($db);

dol_htmloutput_mesg($mesg);

$linkback='<a href="'.DOL_URL_ROOT.'/admin/modules.php">'.$langs->trans("BackToModuleList").'</a>';
print_fiche_titre($langs->trans("MigToPGSetupDesc"),$linkback,'setup');


// Configuration header
$head = migtopg_admin_prepare_head();
dol_fiche_head($head, 'settings', $langs->trans("Module104000Name"), 0,"migtopg@migtopg");
// Admin var of module
print_titre($langs->trans("MigToPGAdmVar"));


print '<table class="noborder" width="100%">';

if ($action=='edit') {
	print '<form method="post" action="'.$_SERVER['PHP_SELF'].'" enctype="multipart/form-data" >';
	print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
	print '<input type="hidden" name="action" value="update">';
}

print '<tr class="liste_titre">';
print '<td width="40%">'.$langs->trans("Name").'</td>';
print '<td width="25%">'.$langs->trans("Valeur").'</td>';
print '<td align="left"></td>';
print "</tr>\n";

//Server
print '<tr class="pair"><td>'.$langs->trans("MigToPGServer").'</td>';
print '<td align="left">';
print $formMigToPG->inputControl('MIG_TO_PG_DB_PG_SERVER', 'text', 40, $conf->global->MIG_TO_PG_DB_PG_SERVER, $action);
print '</td><td align="left">';
print $form->textwithpicto('',$langs->trans("ServerAddressDescription"),1,'help');
print '</td>';
print '</tr>';

//Port
print '<tr class="impair"><td>'.$langs->trans("MigToPGPort").'</td>';
print '<td align="left">';
print $formMigToPG->inputControl('MIG_TO_PG_DB_PG_PORT', 'text', 10, $conf->global->MIG_TO_PG_DB_PG_PORT, $action);
print '</td><td align="left">';
print $form->textwithpicto('',$langs->trans("ServerPortDescription"),1,'help');
print '</td>';
print '</tr>';

//DB Name
print '<tr class="pair"><td>'.$langs->trans("DatabaseName").'</td>';
print '<td align="left">';
print $formMigToPG->inputControl('MIG_TO_PG_DB_NAME', 'text', 40, $conf->global->MIG_TO_PG_DB_NAME, $action);
print '</td><td align="left">';
print $form->textwithpicto('',$langs->trans("MigToPGDatabaseNameHelp"),1,'help');
print '</td>';
print '</tr>';

//User Login
print '<tr class="impair"><td>'.$langs->trans("MigToPGLogin").'</td>';
print '<td align="left">';
print $formMigToPG->inputControl('MIG_TO_PG_DB_USER', 'text', 40, $conf->global->MIG_TO_PG_DB_USER, $action);
print '</td><td align="left">';
print $form->textwithpicto('',$langs->trans("MigToPGAdminLoginHelp"),1,'help');
print '</td>';
print '</tr>';

//Password
print '<tr class="pair"><td>'.$langs->trans("MigToPGPassword").'</td>';
print '<td align="left">';
print $formMigToPG->inputControl('MIG_TO_PG_DB_PASS', 'password', 40, $conf->global->MIG_TO_PG_DB_PASS, $action);
print '</td><td align="left">';
print $form->textwithpicto('',$langs->trans("MigToPGAdminPasswordHelp"),1,'help');
print '</td>';
print '</tr>';

//User Login
print '<tr class="impair"><td>'.$langs->trans("MigToPGRootLogin").'</td>';
print '<td align="left">';
print $formMigToPG->inputControl('MIG_TO_PG_DB_RUSER', 'text', 40, $conf->global->MIG_TO_PG_DB_RUSER, $action);
print '</td><td align="left">';
print $form->textwithpicto('',$langs->trans("MigToPGAdminRootLoginHelp"),1,'help');
print '</td>';
print '</tr>';

//Password
print '<tr class="pair"><td>'.$langs->trans("MigToPGRootPassword").'</td>';
print '<td align="left">';
print $formMigToPG->inputControl('MIG_TO_PG_DB_RPASS', 'password', 40, $conf->global->MIG_TO_PG_DB_RPASS, $action);
print '</td><td align="left">';
print $form->textwithpicto('',$langs->trans("MigToPGAdminRootPasswordHelp"),1,'help');
print '</td>';
print '</tr>';

print '</table>';

if ($action=='edit') {
	
	print '<br><center>';
	print '<input class="button" type="submit" name="save" value="'.$langs->trans("Save").'">';
	print ' &nbsp; &nbsp; ';
	print '<input class="button" type="submit" name="cancel" value="'.$langs->trans("Cancel").'">';
	print '</center>';
	print '</form>';
	
}
elseif ($action=='view') {
	// Boutons actions
	print '<div class="tabsAction">';
	
	print '<a class="butAction" href="'.$_SERVER["PHP_SELF"].'?action=edit">'.$langs->trans("Modify").'</a>';
	
	if ($user->rights->migtopg->execmig)
	{
		print '<a class="butAction" href="../exec/migtopg_step1.php?action=check">'.$langs->trans("MigToPGExecuteStep1").'</a>';
	}
	else
	{
		print '<a class="butActionRefused" href="#" title="'.$langs->trans("MigToPGExecuteStep1NoTest").'">'.$langs->trans("MigToPGExecuteStep1NoTest").'</a>';
	}
	
	print '</div>';
}

$db->close();

llxFooter();