MigToPG : Migrate Dolibarr on MySQL To PostgreSQL
=========

Orginally Dolibarr is build for the SGBD(database engine) MySQL.
On First Dolibarr install it's allow to install on PostgreSQL.

This module is build to transfert your data from MySQL to PostgreSQL on existing installation.

Of course ,you first need to have PostgreSQL install on your server.

This module was tested MySQL 5.5 and migration to PostgreSQL 9.3
But there is no restriction to start from MySQL 5.0 to PostgreSQL 9.3

As Dolibarr is compatible with postgreSQL nativly, it not the case for all external module.
This module will not tranform/adapt external module. There is no garanty concerning full relevent migration for external module.

Make a backup of your orignal /htdocs/conf/conf.php before began the migration.
If it failed you can always put the old conf.php file to retreive your original environement.

This module will not change any data in your original database.

This module is publish under licence GPL3 (see COPYING file)

Contact : florian.henry@open-concept.pro
Website : www.open-concept.pro

--------------------------------------

Dolibarr a été concus pour le moteur de base de donnée MySQL.
A l'instalation Dolibarr propose une installation sur PostgreSQL.

Ce module a été concue pour migré vos donnée de MySQL vers PostgreSQL sur une installation existante.

Biensur, vous devez a avoir un serveur PostgreSQL fonctionnel avant de commancer la migration.

Ce Module a été developper et tester avec MySQL 5.5 et PostgreSQL 9.3.
Cependant, il devrait être compatible avec MySQL 5.0 et PostgreSQL 9.3

De plus Dolibarr est prévue pour fonctionner avec PostgreSQL, mais ce n'est pas le cas des modules externes.
Ce module ne change pas les module externe a Dolibarr. Il n'y a pas de garantie de fonctionnement des modules externe sur PostgreSQL si il n'ont pas été developer en conséquance.

Faite une copie de sauvegarde du fichier /htdocs/conf/conf.php avant de commencer la migration
Si la migration échoue, ou si vous voulez retrouner sur MySQL vous pourrez remettre cette copie a sa place pour retrouver votre environement d'origine.

Ce module ne modifie pas les donnée de votre base de donnée d'origine.

Ce module est sous license GPL3 (voir le fichier COPYING)

Contact : florian.henry@open-concept.pro
Website : www.open-concept.pro
