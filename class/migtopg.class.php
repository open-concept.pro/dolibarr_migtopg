<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
 *
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
*  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
*  \file       htdocs/custom/migtopg/class/migtopg.class.php
*  \ingroup    MigToPG
*  \brief      Class use to main opeartion
*/

require_once(DOL_DOCUMENT_ROOT .'/core/db/pgsql.class.php');

/**
 *	\class      DoliDBPgsql Migration to Postgresql extansion
 *	\brief      Class to drive a Postgresql database for Dolibarr
 */
class MigToPG extends DoliDBPgsql
{
	
	//Host of postgreSQL server
	var $pg_host;
	//Port of postgreSQL server
	var $pg_port;
	//User of postgreSQL server
	var $pg_user;
	//password of postgreSQL server
	var $pg_password;
	//Dolibarr trget database name
	var $pg_db_name;
	//Admin user of postgreSQL server
	var $pg_root_user;
	//Admin password of postgreSQL server
	var $pg_root_password;
	//Table with dolibarr prefix table exists inside
	var $dbexists_withtbl; 
	//User connexion is db owner
	var $user_role_owner;
	//User connexion role is create db
	var $user_role_credb;	
	
	//! collation type
	var $forcedctype;
	
	/**
	 *	Constructor.
	 *
	 *	@return	    int					1 if OK, 0 if not
	 */
	function __construct()
	{
		$this->dbexists_withtbl=0;
		$this->user_role_owner=0;
		$this->user_role_credb=0;
		$this->versionmin=array(9,1,0);
	}
	
	/**
	 *	Connect to PostgresSql Database
	 *
	 *	You need to close connection after call of this method
	 *
	 *	@param	    string	$host		Address of database server
	 *	@param	    string	$user		Nom de l'utilisateur autorise
	 *	@param	    string	$pass		Mot de passe
	 *	@param	    string	$name		Nom de la database
	 *	@param	    int		$port		Port of database server
	 *	@return	    int					1 no error, -1 if errors
	 *
	 */
	function connectPgSQL($host, $dbuser, $dbpass, $dbname, $port) {
		$ret=@parent::__construct('pgsql', $host, $dbuser, $dbpass, $dbname, $port);
		if ($ret<0) {
			$this->error = $langs->trans("MigToPGDBConnectErr",$dbname) .' or '.$langs->trans("MigToPGConnectErr",$host) .' '. $this->error;
			return -1;
		}else {
			return 1;
		}
	}
	
	/**
	 *	Test connexion with the server and parameters of this class
	 *
	 *	You need to close connection after call of this method
	 *	
	 *	@param		string	$type		"server" will test only server, "database" connect to dolibarr db, "admin" try with admin db user
	 *	@return	    int					1 no error, -1 if errors
	 *
	 */
	function testConnexion($type='database') 
	{
		global $langs;
		$dbname='';
		$dbuser=$this->pg_user;
		$dbpass=$this->pg_password;
		
		if ($type=='database') {
			$dbname=$this->pg_db_name;
		}
		if ($type=='admin') {
			$dbuser=$this->pg_root_user;
			$dbpass=$this->pg_root_password;
		}
		
		$ret=@parent::__construct('pgsql', $this->pg_host, $dbuser, $dbpass, $dbname, $this->pg_port);
		if ($ret<0) {
			if ($type=='database') {
				$this->error = $langs->trans("MigToPGDBConnectErr",$dbname) .' '. $this->error;
			}else {
				$this->error = $langs->trans("MigToPGConnectErr",$this->pg_host) .' '. $this->error;
			}
			$error++;
		}else { 
			if ($this->connected != 1)	{
				if ($type=='database') {
					$this->error = $langs->trans("MigToPGDBConnectErr",$dbname) .' '. $this->error;
				}else {
					$this->error = $langs->trans("MigToPGConnectErr",$this->pg_host) .' '. $this->error;
				}
				$error++;
			}else {
				if ($type=='database' && $this->database_selected != 1) {
					$this->error = $langs->trans("MigToPGDBConnectErr",$dbname) .' '. $this->error;
					$error++;
				}
			}
		}
		
		if ($error) {
			return -1;
		}
		else {
			return 1;
		}
	}
	
	/**
	 *	Determine if table with dolibarr prefix table exists inside
	 *	
	 *	@return	    int		number of table, -1 if errors
	 *	
	 */
	function isDolibarrTableExist()
	{
		$error=0;
		
		//determine if table with dolibarr prefix table exists inside
		$sql="SELECT table_name FROM information_schema.tables ";
		$sql.="WHERE table_schema='public' ";
		$sql.="AND table_catalog='".$this->pg_db_name."' ";
		$sql.="AND table_name LIKE '".MAIN_DB_PREFIX."%'";
		
		dol_syslog(get_class($this)."::is_dolibarr_table_exist sql=".$sql, LOG_DEBUG);
		$resql=$this->query($sql);
		if ($resql) {
			
			$num=$this->num_rows($resql);
			
			if ($num>0) {
				$this->dbexists_withtbl=1;
			}
				
			$this->free($resql);
		}
		else {
			$this->error="Error ".$this->lasterror();
			$error++;
			dol_syslog(get_class($this)."::is_dolibarr_table_exist sql=".$sql, LOG_ERROR);
		}
		
		if ($error) {
			return -1;
		}
		else {
			return $num;
		}
	} 
	
	
	/**
	 *	Test if user connexion is db owner
	 *
	 *	@return	    int		1 no error, -1 if errors
	 *
	 */
	function isUserIsDbowner()
	{
		$error=0;
		
		//Test if user connexion is db owner
		$sql="SELECT usename fROM pg_database db ";
		$sql.="INNER JOIN pg_user usr ON db.datdba=usr.usesysid ";
		$sql.="WHERE db.datname='".$this->pg_db_name."' ";
		$sql.="AND usr.usename='".$this->pg_user."'";
		
		dol_syslog(get_class($this)."::is_user_is_dbowner sql=".$sql, LOG_DEBUG);
		$resql=$this->query($sql);
		if ($resql) {
			if ($this->num_rows($resql)) {
				$this->user_role_owner=1;
			}
				
			$this->free($resql);
		}
		else {
			$this->error="Error ".$this->lasterror();
			$error++;
			dol_syslog(get_class($this)."::is_user_is_dbowner sql=".$sql, LOG_ERROR);
		}
		
		if ($error) {
			return -1;
		}
		else {
			return 1;
		}
	}
	
	/**
	 *	Test if user connexion have to createdb
	 *
	 *	@param		string	$type		"normal" try with db user, "admin" try with admin db user
	 *	@return	    int		1 no error, -1 if errors
	 *
	 */
	function isUserCreatedbRole($type='normal')
	{
		$error=0;
		
		//Test if user connexion get createdb role
		$sql="SELECT usr.usename fROM pg_user usr ";
		$sql.="WHERE usr.usecreatedb=TRUE ";
		if ($type=='normal') {
			$sql.="AND usr.usename='".$this->pg_user."'";
		}elseif ($type=='admin') {
			$sql.="AND usr.usename='".$this->pg_root_user."'";
		}
		
		dol_syslog(get_class($this)."::is_user_createdb_role sql=".$sql, LOG_DEBUG);
		
		$resql=$this->query($sql);
		if ($resql) {
			if ($this->num_rows($resql)) {
				$this->user_role_credb=1;
			}
				
			$this->free($resql);
		}
		else
		{
			$this->error="Error ".$this->lasterror();
			$error++;
			dol_syslog(get_class($this)."::is_user_createdb_role sql=".$sql, LOG_ERROR);
		}
		
		if ($error) {
			return -1;
		}
		else {
			return 1;
		}
	}
	
	/**
	 *	Create a new database
	 *	Do not use function xxx_create_db (xxx=mysql, ...) as they are deprecated
	 *	We force to create database with charset this->forcecharset and collate this->forcecollate
	 *
	 *	@param	string	$database		Database name to create
	 * 	@param	string	$charset		Charset used to store data
	 * 	@param	string	$collation		Charset used to sort data
	 * 	@param	string	$ctype			Charset used to sort upper/lower case, interger
	 * 	@param	string	$owner			Username of database owner
	 * 	@return	resource				resource defined if OK, null if KO
	 */
	function migtopg_DDLCreateDb($database,$charset,$collation,$ctype,$owner)
	{
		$sql='CREATE DATABASE '.$database.' ';
		$sql.='WITH OWNER = '.$owner.' ';
		$sql.='ENCODING = \''.$charset.'\'';
		$sql.='	LC_COLLATE = \''.$collation.'\'';
		$sql.='	LC_CTYPE = \''.$ctype.'\'';
		$sql.=' CONNECTION LIMIT = -1';
		
		dol_syslog(get_class($this)."::migtopg_DDLCreateDb sql=".$sql, LOG_DEBUG);
		$ret=$this->query($sql);
		if ($ret) {
			return $ret;
		}else {
			$this->error="Error ".$this->lasterror();
			return -1;
		}
		
	}
	
	/**
	 *	Return ctype  used in database
	 *
	 *	@return		string		cType value
	 */
	function getDefaultCTypeDatabase()
	{
		$resql=$this->query('SHOW LC_CTYPE');
		dol_syslog(get_class($this)."::getDefaultCTypeDatabase sql=".$sql, LOG_DEBUG);
		$liste=$this->fetch_array($resql);
		return $liste['lc_ctype'];
	}
	
	/**
	 *	Set line array of choice for collation and ctype
	 *
	 *	@return	    int		number of rows return by request, -1 if errors
	 *
	 */
	function getListCollationAvalaible()
	{
		$error=0;
	
		//Test if user connexion get createdb role
		$sql="SELECT collation_name || '.' || ";
		$sql.="(CASE ";
		$sql.="WHEN character_set_name='UTF8' THEN 'UTF-8' ";
		$sql.="	ELSE character_set_name ";
		$sql.="	END) as dbcolation FROM information_schema.collation_character_set_applicability ";
		$sql.="	WHERE collation_name NOT LIKE '%.%' ";
		$sql.="	AND collation_name NOT IN ('default','C','POSIX')";
		
		dol_syslog(get_class($this)."::getCollationAvalaible sql=".$sql, LOG_DEBUG);
	
		$resql=$this->query($sql);
		if ($resql)
		{
			$this->line_dbcolation = array();
			$num = $this->num_rows($resql);
			$i = 0;
		
			if ($num)
			{
				while( $i < $num)
				{
					$obj = $this->fetch_object($resql);
					$this->line_dbcolation[$i] = $obj->dbcolation;
					$i++;
				}
			}
			$this->free($resql);
			return $num;
		}
		else
		{
			$this->error="Error ".$this->lasterror();
			dol_syslog(get_class($this)."::getCollationAvalaible ".$this->error, LOG_ERR);
			return -1;
		}
	}
	
/**
	 *	Set line array of choice for charset
	 *
	 *	@return	    int		number of rows return by request, -1 if errors
	 *
	 */
	function getListCharsetAvalaible()
	{
		$error=0;
	
		//Test if user connexion get createdb role
		$sql="SELECT DISTINCT character_set_name  as dbcharset FROM information_schema.collation_character_set_applicability ";
		$sql.="WHERE collation_name NOT LIKE '%.%' ";
		$sql.="AND collation_name NOT IN ('default','C','POSIX') ";
		
		dol_syslog(get_class($this)."::getListCharsetAvalaible sql=".$sql, LOG_DEBUG);
	
		$resql=$this->query($sql);
		if ($resql)
		{
			$this->line_dbcharset = array();
			$num = $this->num_rows($resql);
			$i = 0;
		
			if ($num)
			{
				while( $i < $num)
				{
					$obj = $this->fetch_object($resql);
					$this->line_dbcharset[$i] = $obj->dbcharset;
					$i++;
				}
			}
			$this->free($resql);
			return $num;
		}
		else
		{
			$this->error="Error ".$this->lasterror();
			dol_syslog(get_class($this)."::getListCharsetAvalaible ".$this->error, LOG_ERR);
			return -1;
		}
	}
	
	
	/**
	 *	Drop existing database
	 *
	 *	@return	    int		1 no error, -1 if errors
	 */
	function dropDatabase() {
		
		global $langs;
		
		$error = 0;
		
		$ret=@parent::__construct('pgsql', $this->pg_host, $this->pg_root_user, $this->pg_root_password, '', $this->pg_port);
		if ($ret>0) {
			
			$sql = 'DROP DATABASE '.$this->pg_db_name;
			
			dol_syslog(get_class($this)."::dropDatabase sql=".$sql, LOG_DEBUG);
			
			$resql=$this->query($sql);
			
			if ($resql) {
				$this->free($resql);
			}
			else {
				$this->error=$langs->trans("MigToPGCannotDropDB").' '.$this->lasterror();
				$error++;
				dol_syslog(get_class($this)."::dropDatabase Error=".$this->error, LOG_ERROR);
			}
			
			$ret = $this->close();
			if (!$ret) {
				dol_syslog(get_class($this)."::dropDatabase disconnect", LOG_ERROR);
				$this->error =$langs->trans("MigToPGDisconnectErr",$MigToPg_db->pg_host);
				$error++;
			}
		}else {
			$this->error = $langs->trans("MigToPGDBConnectErr",$dbname) .' '. $this->error;
			$error++;
		}
		
		if ($error) {
			return -1;
		}
		else {
			return 1;
		}
	}	
}