<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
 *
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
*  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
*  \file       htdocs/custom/migtopg/class/migtopg_structures.class.php
*  \ingroup    MigToPG
*  \brief      Class use to get tables structures
*/
require_once('migtopg_tbl.class.php');

/**
 *	\class      DoliDBPgsql Migration to Postgresql extansion
*	\brief      Class to drive a Postgresql database for Dolibarr
*/
class MigToPG_MySQLStructure
{
	var $db;
	var $mysql_db_name;
	var $mysql_tables;
	var $error;
	var $de_dbl_constraint_name;
	
	var $sql_build_tables;
	var $sql_insert;
	var $sql_field_autoupdate;
	
	/**
	 *	Constructor.
	 *
	 *  @param		DoliDB		$db      Database handler
	 *	@return	    int					1 if OK, 0 if not
	*/
	function __construct($db)
	{
		$this->db=$db;
		$this->mysql_db_name=$db->database_name;
		$this->mysql_tables=array();
		$this->de_dbl_constraint_name=array();
		return 1;
	}
	
	/**
	 *	Set list of mysql tables
	 *
	 *	@param		bool	$populate_fields      populate field structure inside
	 *	@param		bool	$populate_constraints populate constraint inside
	 *	@param		bool	$populate_index      populate index inside
	 *	@return	    int		number of row, -1 if errors
	 *
	 */
	function getListTables($populate_fields=false, $populate_constraints=false, $populate_index=false)
	{
		$error=0;
	
		//determine if table with dolibarr prefix table exists inside
		$sql="SHOW TABLES IN ". $this->mysql_db_name;

		dol_syslog(get_class($this)."::getListTables sql=".$sql, LOG_DEBUG);
		$resql=$this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);
			$i = 0;
			$table_with_prefix=0;
				
			if ($num)
			{
				while (($i < $num) && $error==0)
				{
					$arr_tbl = $this->db->fetch_array($resql);
					
					//We include in schema only table with Dolibarr tables prefix
					if (strpos($arr_tbl[0],MAIN_DB_PREFIX)!==false) {

						$this->mysql_tables[$i]=new MigToPGTables($arr_tbl[0]);
						
						if ($populate_fields) {
							$ret=$this->getListField($this->mysql_tables[$i]);
							if ($ret<0) {
								$error++;
							}
						}
						
						if ($populate_constraints) {
							$ret=$this->getListConstraint($this->mysql_tables[$i]);
							if ($ret<0) {
								$error++;
							}
						}
						
						if ($populate_index) {
							$ret=$this->getListIndex($this->mysql_tables[$i]);
							if ($ret<0) {
								$error++;
							}
						}
						
						$table_with_prefix++;
					}
					$i++;
				}
			}else {
				$this->error="Error Cannot list tables";
				$error++;
				dol_syslog(get_class($this)."::getListTables ".$this->error." sql=".$sql, LOG_ERROR);
			}
	
			$this->db->free($resql);
			
			if ($populate_constraints || $populate_index) {
				$ret=$this->removeDoublon();
				if ($ret<0) {
					$error++;
				}
			}
		}
		else {
			$this->error="Error ".$this->db->lasterror();
			$error++;
			dol_syslog(get_class($this)."::getListTables sql=".$sql, LOG_ERROR);
		}
	
		if ($error) {
			return -1;
		}
		else {
			return $table_with_prefix;
		}
	}
	
	
	/**
	 *	Create a object table for deduplicate constraint or index name
	 *
	 *	@return	    int					number of row, -1 if errors
	 *
	 */
	function removeDoublon()
	{
		$i=0;
		$index_name_unique=array();
		foreach ($this->mysql_tables as $tables) {
			if (!empty($tables->indexs)) {
				foreach ($tables->indexs as $index) {
					if (!in_array($index->index_name,$index_name_unique)) {
						$index_name_unique[]=$index->index_name;
					} else {
						$new_index_name='idx_'.$index->index_name.$i.'_'.str_replace(MAIN_DB_PREFIX,'',$tables->table_name);
						$this->de_dbl_constraint_name[] = new MigToPGDuplicateIndexName($tables->table_name,$index->index_name,$new_index_name,'Index');
						$i++;
					}		
				}
			}
		}
		
		$i=0;
		$constraint_name_unique=array();
		foreach ($this->mysql_tables as $tables) {
			if (!empty($tables->constraints)) {
				foreach ($tables->constraints as $constraint) {
					if ($constraint->type!=$constraint->types[0]) {
						if (!in_array($constraint->constraint_name,$constraint_name_unique)) {
							$constraint_name_unique[]=$constraint->constraint_name;
						} else {
							$prefix='idx';
							if ($constraint->type==$constraint->types[1]) {$prefix='uk';}
							if ($constraint->type==$constraint->types[2]) {$prefix='fk';}	
							$new_constraint_name=$prefix.'_'.$constraint->constraint_name.$i.'_'.str_replace(MAIN_DB_PREFIX,'',$tables->table_name);
							$this->de_dbl_constraint_name[] = new MigToPGDuplicateIndexName($tables->table_name,$constraint->constraint_name,$new_constraint_name,'Constraint');
							$i++;
						}
					}
				}
			}
		}
		
		
	}
	
	/**
	 *	Set list of field for tables
	 *
	 *  @param		string		$table 	table obejct
	 *	@return	    int					number of row, -1 if errors
	 *
	 */
	function getListField($table)
	{
		$error=0;
	
		//determine if table with dolibarr prefix table exists inside
		$sql="SHOW FULL COLUMNS FROM ". $table->table_name;
	
		dol_syslog(get_class($this)."::getListField sql=".$sql, LOG_DEBUG);
		$resql=$this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);
			$i = 0;
	
			if ($num)
			{
				while( $i < $num)
				{
					$obj = $this->db->fetch_object($resql);
					$table->fields[$i] = new MigToPGField($obj->Field);
					$table->fields[$i]->type=$obj->Type;
					if ($obj->Null=='YES') {$table->fields[$i]->can_be_null=true;}
					$table->fields[$i]->default_value=$obj->Default;
					if ($obj->Key=='PRI') {$table->fields[$i]->is_PK=true;}
					if ($obj->Extra=='auto_increment') {$table->fields[$i]->is_autoincrement=true;}
					if ($obj->Extra=='on update CURRENT_TIMESTAMP') {$table->fields[$i]->is_auto_update_CTS=true;}
					if ($obj->Comment) {$table->fields[$i]->comment=$obj->Comment;}
					$i++;
				}
			}
	
			$this->db->free($resql);
		}
		else {
			$this->error="Error ".$this->db->lasterror();
			$error++;
			dol_syslog(get_class($this)."::getListField sql=".$sql, LOG_ERROR);
		}
	
		if ($error) {
			return -1;
		}
		else {
			return $num;
		}
	}
	
	/**
	 *	Set list of contraint for tables
	 *
	 *  @param		string		$table 	table object
	 *	@return	    int					number of row, -1 if errors
	 *
	 */
	function getListConstraint($table)
	{
		$error=0;
	
		//determine if table with dolibarr prefix table exists inside
		$sql = 'SELECT CONSTRAINT_CATALOG, ';
		$sql.= 'CONSTRAINT_SCHEMA, ';
		$sql.= 'CONSTRAINT_NAME, ';
		$sql.= 'TABLE_CATALOG, ';
		$sql.= 'TABLE_SCHEMA, ';
		$sql.= 'TABLE_NAME, ';
		$sql.= 'COLUMN_NAME, ';
		$sql.= 'ORDINAL_POSITION, ';
		$sql.= 'POSITION_IN_UNIQUE_CONSTRAINT, ';
		$sql.= 'REFERENCED_TABLE_SCHEMA, ';
		$sql.= 'REFERENCED_TABLE_NAME, ';
		$sql.= 'REFERENCED_COLUMN_NAME ';
    	$sql.=' FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE ';
    	$sql.=" WHERE TABLE_NAME = '".$table->table_name."'";
    	$sql.=" AND CONSTRAINT_SCHEMA = '".$this->mysql_db_name."'";
    	$sql.=" ORDER BY CONSTRAINT_NAME, ORDINAL_POSITION";
	
		dol_syslog(get_class($this)."::getListConstraint sql=".$sql, LOG_DEBUG);
		$resql=$this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);
			$i = 0;
	
			if ($num)
			{
				while( $i < $num)
				{
					$obj = $this->db->fetch_object($resql);
					//Create the object if needed, may be created in prevouis loop
					if (empty($table->constraints[$obj->CONSTRAINT_NAME])) {
						$table->constraints[$obj->CONSTRAINT_NAME] = new MigToPGConstraint($obj->CONSTRAINT_NAME);
					}
					
					if ($obj->CONSTRAINT_NAME=='PRIMARY') {
						//Primary key
						$table->constraints[$obj->CONSTRAINT_NAME]->type=$table->constraints[$obj->CONSTRAINT_NAME]->types[0];
					} elseif (!empty($obj->REFERENCED_TABLE_NAME) && !empty($obj->REFERENCED_COLUMN_NAME)){
						//Foreign key
						$table->constraints[$obj->CONSTRAINT_NAME]->type=$table->constraints[$obj->CONSTRAINT_NAME]->types[2];
						$table->constraints[$obj->CONSTRAINT_NAME]->fields_target['table']=$obj->REFERENCED_TABLE_NAME;
						$table->constraints[$obj->CONSTRAINT_NAME]->fields_target['field']=$obj->REFERENCED_COLUMN_NAME;
						
						//We don't have other choice to go throu CREATE TABLE script 
						//to get ON DELETE and ON UPDATE rules 
						//Seems that dolibarr base sql script do not use this clause
						//but I use them in some of my modules so... I have to code to take it account
						$sql_fk='SHOW CREATE TABLE '.$table->table_name;
						dol_syslog(get_class($this)."::getListConstraint sql_fk=".$sql_fk, LOG_DEBUG);
						$resql_fk=$this->db->query($sql_fk);
						if ($resql_fk) {
							$num_fk = $this->db->num_rows($resql_fk);
							if ($num_fk) {
								$arr_fk = $this->db->fetch_array($resql_fk);
								$createtable = $arr_fk['Create Table'];
								$createtable_arr=explode("\n",$createtable);
								//In the Create table script we locate line for the current CONTRAINT Foreign  key
								foreach($createtable_arr as $cre_tbl_line) {
									if ((strpos($cre_tbl_line,'CONSTRAINT')!==false) 
										&& (strpos($cre_tbl_line,$obj->CONSTRAINT_NAME)!==false)
										&& (strpos($cre_tbl_line,' ON ')!==false)) {
										
										$on_clause_arr=explode(' ON ',$cre_tbl_line);
										foreach($on_clause_arr as $on_clause_str) {
											//To be sure we parse only line of ON DELETE or ON UPDATE we excluse constraint line 
											if (strpos($on_clause_str,'CONSTRAINT')===false) {
												if (strpos($on_clause_str,'DELETE')!==false) {
													if (strpos($on_clause_str,'CASCADE')!==false) {
														$table->constraints[$obj->CONSTRAINT_NAME]->on_DELETE=$table->constraints[$obj->CONSTRAINT_NAME]->on_clauses[2];
													}elseif (strpos($on_clause_str,'RESTRICT')!==false) {
														$table->constraints[$obj->CONSTRAINT_NAME]->on_DELETE=$table->constraints[$obj->CONSTRAINT_NAME]->on_clauses[1];
													}elseif (strpos($on_clause_str,'SET NULL')!==false) {
														$table->constraints[$obj->CONSTRAINT_NAME]->on_DELETE=$table->constraints[$obj->CONSTRAINT_NAME]->on_clauses[3];
													}elseif (strpos($on_clause_str,'NO ACTION')!==false) {
														$table->constraints[$obj->CONSTRAINT_NAME]->on_DELETE=$table->constraints[$obj->CONSTRAINT_NAME]->on_clauses[0];
													}
												}
												
												if (strpos($on_clause_str,'UPDATE')!==false) {
													if (strpos($on_clause_str,'CASCADE')!==false) {
														$table->constraints[$obj->CONSTRAINT_NAME]->on_UPDATE=$table->constraints[$obj->CONSTRAINT_NAME]->on_clauses[2];
													}elseif (strpos($on_clause_str,'RESTRICT')!==false) {
														$table->constraints[$obj->CONSTRAINT_NAME]->on_UPDATE=$table->constraints[$obj->CONSTRAINT_NAME]->on_clauses[1];
													}elseif (strpos($on_clause_str,'SET NULL')!==false) {
														$table->constraints[$obj->CONSTRAINT_NAME]->on_UPDATE=$table->constraints[$obj->CONSTRAINT_NAME]->on_clauses[3];
													}elseif (strpos($on_clause_str,'NO ACTION')!==false) {
														$table->constraints[$obj->CONSTRAINT_NAME]->on_UPDATE=$table->constraints[$obj->CONSTRAINT_NAME]->on_clauses[0];
													}
												}
											}
										}
										
									}
								}
							}
							
							$this->db->free($resql_fk);
						}
						
						//Default is NO ACTION
						if (empty($table->constraints[$obj->CONSTRAINT_NAME]->on_DELETE)) {
							$table->constraints[$obj->CONSTRAINT_NAME]->on_DELETE=$table->constraints[$obj->CONSTRAINT_NAME]->on_clauses[0];
						}
						//Default is NO ACTION
						if (empty($table->constraints[$obj->CONSTRAINT_NAME]->on_UPDATE)) {
							$table->constraints[$obj->CONSTRAINT_NAME]->on_UPDATE=$table->constraints[$obj->CONSTRAINT_NAME]->on_clauses[0];
						}
						
						
						
					} else {
						//User key
						$table->constraints[$obj->CONSTRAINT_NAME]->type=$table->constraints[$obj->CONSTRAINT_NAME]->types[1];
					}
					
					$table->constraints[$obj->CONSTRAINT_NAME]->fields[$obj->ORDINAL_POSITION]=$obj->COLUMN_NAME;
					
					$i++;
				}
			}
	
			$this->db->free($resql);
		}
		else {
			$this->error="Error ".$this->db->lasterror();
			$error++;
			dol_syslog(get_class($this)."::getListConstraint sql=".$sql, LOG_ERROR);
		}
	
		if ($error) {
			return -1;
		}
		else {
			return $num;
		}
	}
	
	/**
	 *	Set list of index not unique and no foreign key for tables
	 *
	 *  @param		string		$table_name 	table object
	 *	@return	    int					number of row, -1 if errors
	 *
	 */
	function getListIndex($table)
	{
		$error=0;
	
		//determine if table with dolibarr prefix table exists inside
		$sql="SHOW INDEX FROM ". $table->table_name;
	
		dol_syslog(get_class($this)."::getListIndex sql=".$sql, LOG_DEBUG);
		$resql=$this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);
			$i = 0;
	
			if ($num)
			{
				while( $i < $num)
				{
					$obj = $this->db->fetch_object($resql);
					//Populate only not unique and not fk index
					if (($obj->Non_unique==1) && ($obj->Key_name!='PRIMARY')) {
						//Create the object if needed, may be created in prevouis loop
						if (empty($table->indexs[$obj->Key_name])) {
							$table->indexs[$obj->Key_name] = new MigToPGIndex($obj->Key_name);
						}
						$table->indexs[$obj->Key_name]->fields[$obj->Seq_in_index] = $obj->Column_name;
					}
				
					$i++;
				}
				
			}
	
			$this->db->free($resql);
		}
		else {
			$this->error="Error ".$this->db->lasterror();
			$error++;
			dol_syslog(get_class($this)."::getListIndex sql=".$sql, LOG_ERROR);
		}
	
		if ($error) {
			return -1;
		}
		else {
			return $num;
		}
	}
	
	/**
	 *	Create CREATE TABLE Script
	 *
	 *  @param		string		$db_owner 		Table owner
	 *  @param		string		$script_tbl		Script create table
	 *  @param		string		$script_index 	Script create index
	 *	@return	    string		return 1
	 *
	 */
	function createStructureScriptTables($db_owner, $script_tbl=false, $script_index=false, $script_constraint=false) {
		
		$sql_build_tables='';
		
		//Create tables
		if ($script_tbl) {
			
			foreach ($this->mysql_tables as $tables) {
				
				$sql_PK='';
				$sql_enumtype='';
				$sql_build_tables.='CREATE TABLE '. $tables->table_name."\n";
				$sql_build_tables.='('."\n";
			
				foreach ($tables->fields as $field) {
					
					
					//Field name
					$sql_build_tables.='  '.$field->field_name.' ';
					
					//Field type
					//If field is integer and primary key and autoincrement, must be serial in PostgreSQL
					if ((strpos($field->type,'int')!==false) && ($field->is_PK) && ($field->is_autoincrement)){
						$sql_build_tables.='serial';
					}
					//PostgreSQL type syntax do not permit paranthesis for interger size
					elseif (strpos($field->type,'int')===0) {
						$sql_build_tables.='integer';
					}elseif ((strpos($field->type,'int')!==false) && ((strpos($field->type,'('))!==false)){
						$type=$field->type;
						$type=str_replace('tinyint','smallint',$type);
						$type=str_replace('unsigned','',$type);
						$sql_build_tables.=substr_replace($type,'',strpos($type,'('));
					}
					//datetime type do not exists, only stimestamp in PostgreSQL
					elseif ((strpos($field->type,'datetime')!==false) || (strpos($field->type,'timestamp')!==false)) {
						$sql_build_tables.='timestamp without time zone';
					}
					//Enum type must be declare as a unique type 
					elseif (strpos($field->type,'enum')!==false) {
						preg_match('/\((.*)\)/',$field->type,$matches);
						$enum_list=$matches[1];
						$enumtype=$tables->table_name.'_'.$field->field_name;
						$sql_enumtype.='CREATE TYPE '.$enumtype.' AS ENUM ('.$enum_list.');'."\n";
						$sql_build_tables.=$enumtype;
					}
					//Other type are common between MySQL and PostgreSQL
					else {
						$type=$field->type;
						
						//Copy from pgsql.class.php
						// double -> numeric
						$type=preg_replace('/^double/i','numeric',$type);
						$type=preg_replace('/(\s*)double/i','\\1real',$type);
						// float -> numeric
						$type=preg_replace('/^float/i','numeric',$type);
						$type=preg_replace('/(\s*)float/i','\\1real',$type);
						
						// blob -> text
						$type=preg_replace('/\w*blob/i','text',$type);
							
						// tinytext/mediumtext -> text
						$type=preg_replace('/tinytext/i','text',$type);
						$type=preg_replace('/mediumtext/i','text',$type);
						$type=preg_replace('/longtext/i','text',$type);
						
						$sql_build_tables.=$type;
					}

					//Nullable
					if (!$field->can_be_null) {
						$sql_build_tables.=' NOT NULL';
					} else {
						$sql_build_tables.=' NULL';
					}
					
					//Default value
					if ($field->default_value!='') {
						//Avoid generated script with 0.0000000
						if (is_numeric($field->default_value) && (intval($field->default_value)==0)) {
							$sql_build_tables.=' DEFAULT 0';
						}else {
							if ((!is_numeric($field->default_value)) && ($field->default_value!='CURRENT_TIMESTAMP') && ($field->default_value!='0000-00-00 00:00:00') && ($field->default_value!='1000-01-01 00:00:00')) {
								$sql_build_tables.=' DEFAULT \''.$field->default_value.'\'::character varying';
							}elseif ($field->default_value=='0000-00-00 00:00:00') {
								$sql_build_tables.=' DEFAULT CURRENT_TIMESTAMP';
							}elseif ($field->default_value=='1000-01-01 00:00:00') {
								$sql_build_tables.=' DEFAULT CURRENT_TIMESTAMP';
							}else {
								$sql_build_tables.=' DEFAULT '.$field->default_value;
							}
							
						}
					} 
						
					$sql_build_tables.= ','."\n";
				}
				
				//Primary Key
				$sql_PK_list='';
				foreach ($tables->fields as $field) {
					if ($field->is_PK) {
							$sql_PK_list.=$field->field_name.',';
					}
				}
				
				//Add primary key constraint
				if ($sql_PK_list!='') {
					//Remove the last coma and \n caracters
					$sql_PK_list=substr($sql_PK_list, 0, -1);
					$sql_PK='    CONSTRAINT '.$tables->table_name.'_pkey PRIMARY KEY ('.$sql_PK_list.')';
					$sql_build_tables.=$sql_PK;
				}else {
					//Remove the last coma and \n caracters
					$sql_build_tables=substr($sql_build_tables, 0, -2);
				}
				
				//add again the \n caraters and the close parenthesis 
				$sql_build_tables.= "\n".')'."\n";
				
				$sql_build_tables.='WITH ( '."\n";
				$sql_build_tables.='OIDS=FALSE'."\n";
				$sql_build_tables.=');'."\n";
				$sql_build_tables.='ALTER TABLE '.$tables->table_name."\n";
				$sql_build_tables.='OWNER TO '.$db_owner.';'."\n";
				$sql_build_tables.= "\n";
				
				if (!empty($sql_enumtype)) {
					$sql_build_tables=$sql_enumtype."\n".$sql_build_tables;
				}
			}
		}
		
		if ($script_index) {
			$sql_build_tables_index='';
			foreach ($this->mysql_tables as $tables) {
				if (!empty($tables->indexs)) {
					foreach ($tables->indexs as $index) {
						
						$index_name=$index->index_name;
						//Check duplicate index name
						foreach($this->de_dbl_constraint_name as $unique_name) {
							if (($unique_name->type=='Index') 
								&& ($unique_name->table_name==$tables->table_name)
								&& ($unique_name->old_name==$index->index_name))
								$index_name=$unique_name->new_name;
						}

						$sql_build_tables_index.='CREATE INDEX '.$index_name.' '."\n";
						$sql_build_tables_index.=' ON '.$tables->table_name.' '."\n";
						$sql_build_tables_index.=' USING btree '."\n";
						$sql_build_tables_index.=' (';
						foreach($index->fields as $index_field) {
							$sql_build_tables_index.=$index_field.',';
						}
						//Remove the last coma
						$sql_build_tables_index=substr($sql_build_tables_index, 0, -1);
						$sql_build_tables_index.=');'."\n"."\n";
					}
				}
			}
			
			$sql_build_tables.=$sql_build_tables_index;
		}
	
		
		if ($script_constraint) {
			
			$sql_build_tables_uk='';
			$sql_build_tables_fk='';

			//Blocq for user key only
			foreach ($this->mysql_tables as $tables) {
				if (!empty($tables->constraints)) {
					foreach ($tables->constraints as $constraint) {
						//User Key
						if ($constraint->type==$constraint->types[1]) {
							
							$constraint_name=$constraint->constraint_name;
							//Check duplicate constraint name
							foreach($this->de_dbl_constraint_name as $unique_name) {
								if (($unique_name->type=='Constraint')
									&& ($unique_name->table_name==$tables->table_name)
									&& ($unique_name->old_name==$constraint->constraint_name))
									$constraint_name=$unique_name->new_name;
							}

							$sql_build_tables_uk.='CREATE UNIQUE INDEX '.$constraint_name.' '."\n";
							$sql_build_tables_uk.=' ON '.$tables->table_name.' '."\n";
							$sql_build_tables_uk.=' USING btree '."\n";
							$sql_build_tables_uk.=' (';
							
							foreach($constraint->fields as $constraint_field) {
								$sql_build_tables_uk.=$constraint_field.',';
							}
							
							//Remove the last coma
							$sql_build_tables_uk=substr($sql_build_tables_uk, 0, -1);
							$sql_build_tables_uk.= ');'."\n"."\n";
						}
					}
				}
			}
			
			//Bloq for Foreign key Only
			foreach ($this->mysql_tables as $tables) {
				if (!empty($tables->constraints)) {
					foreach ($tables->constraints as $constraint) {
						//Foreign Key
						if ($constraint->type==$constraint->types[2]) {
							
							$constraint_name=$constraint->constraint_name;
							//Check duplicate constraint name
							foreach($this->de_dbl_constraint_name as $unique_name) {
								if (($unique_name->type=='Constraint')
									&& ($unique_name->table_name==$tables->table_name)
									&& ($unique_name->old_name==$constraint->constraint_name))
									$constraint_name=$unique_name->new_name;
							}
							
							$sql_build_tables_fk.='ALTER TABLE '.$tables->table_name.' '."\n";
							$sql_build_tables_fk.=' ADD CONSTRAINT '.$constraint_name.' FOREIGN KEY ('.$constraint->fields[1].') '."\n";
							$sql_build_tables_fk.=' REFERENCES '.$constraint->fields_target['table'].' ('.$constraint->fields_target['field'].') MATCH SIMPLE '."\n";
							$sql_build_tables_fk.=' ON UPDATE '.$constraint->on_UPDATE.' ON DELETE '.$constraint->on_DELETE.' DEFERRABLE INITIALLY IMMEDIATE;'."\n"."\n";
						}
					}
				}
			}
			
			$sql_build_tables.=$sql_build_tables_uk;
			$sql_build_tables.=$sql_build_tables_fk;
		}
		
		$this->sql_build_tables=$sql_build_tables;
		return 1;
	}
	
	/**
	 *	Create sql insert query
	 *
	 *	@param		string		$table 		table object to generate insert query
	 *	@param		string		$pgsql_db 	Pointer to pgsqldb class
	 *	@return	    string		number of source records, or -1 if error
	 *
	 */
	function generateInsertScript($table,$pgsql_db) {
		
		$this->sql_insert='';
		$sql_field_list='';
		
		foreach ($table->fields as $field) {
			
			if ($field->field_name=='virtual') {
				$sql_field_list.='`'.$field->field_name.'`'.',';
			} else {			
				$sql_field_list.=$field->field_name.',';
			}
		}
		//Remove the last coma
		$sql_field_list=substr($sql_field_list, 0, -1);
		
		//determine if table with dolibarr prefix table exists inside
		$sql='SELECT '.$sql_field_list.' FROM '. $table->table_name;
		
		dol_syslog(get_class($this)."::generateInsertScript sql=".$sql, LOG_DEBUG);
		$resql=$this->db->query($sql);
		if ($resql) {
			$num = $this->db->num_rows($resql);
			$i = 0;
			if ($num)
			{	
				$sql_field_list=str_replace('`', '', $sql_field_list);
				
				$this->sql_insert='INSERT INTO '.$table->table_name.' ('.$sql_field_list.') VALUES '."\n";
				
				while( $i < $num)
				{	
					
					$obj = $this->db->fetch_array($resql);
					
					$this->sql_insert.='(';
					foreach ($table->fields as $field) {
						if (is_null($obj[$field->field_name])) {
							$this->sql_insert.='NULL';
						}else {
							if ((strpos($field->type,'text')!==false)
								|| (strpos($field->type,'char')!==false)
								|| (strpos($field->type,'blob')!==false)) {
								$this->sql_insert.='\''.$pgsql_db->escape($obj[$field->field_name]).'\'';
							}elseif ((strpos($field->type,'date')!==false)
								|| (strpos($field->type,'time')!==false)) {
									$this->sql_insert.="'".$pgsql_db->idate($pgsql_db->jdate($obj[$field->field_name]))."'";						
							}elseif ((strpos($field->type,'enum')!==false)) {
								if (!empty($obj[$field->field_name])) {
									$this->sql_insert.='\''.$pgsql_db->escape($obj[$field->field_name]).'\'';	
								}else {
									$this->sql_insert.='NULL';
								}
							}else {
								$this->sql_insert.=$obj[$field->field_name];
							}
						}
	
						$this->sql_insert.=',';
					}
					//Remove the last coma
					$this->sql_insert=substr($this->sql_insert, 0, -1);
					$this->sql_insert.='),'."\n";
					$i++;
				}
			
				//Remove the last coma and \n cararters
				$this->sql_insert=substr($this->sql_insert, 0, -2);
			}

			$this->db->free($resql);
		}else {
			$this->error="Error ".$this->db->lasterror();
			$error++;
			dol_syslog(get_class($this)."::generateInsertScript sql=".$sql, LOG_ERROR);
		}
	
		if ($error) {
			return -1;
		}
		else {
			return $num;
		}
	}
	
	/**
	 *	Create auto-update current time stamp field field
	 *
	 *	@return	    string		1 if ok, -1 if error
	 *
	 */
	function createScriptSpecialField(){
		
		$error=0;
		
		$field_array_CTS=array();
		
		$sql_field_autoupdate='';
		
		foreach ($this->mysql_tables as $table) {
			foreach ($table->fields as $field) {
				if ($field->is_auto_update_CTS) {
					//Create the tigger for each named field
					if (!in_array($field->field_name,$field_array_CTS)) {
						$field_array_CTS[]=$field->field_name;
						$sql_field_autoupdate.='CREATE OR REPLACE FUNCTION update_modified_column_'.$field->field_name.'()	RETURNS TRIGGER AS $$ BEGIN NEW.'.$field->field_name.' = now(); RETURN NEW; END; $$ LANGUAGE plpgsql;'."\n";
					}
					$sql_field_autoupdate.='CREATE TRIGGER update_customer_modtime BEFORE UPDATE ON '.$table->table_name.' FOR EACH ROW EXECUTE PROCEDURE update_modified_column_'.$field->field_name.'();'."\n";
				}
			}
		}

		$this->sql_field_autoupdate=$sql_field_autoupdate;
		
		if ($error) {
			return -1;
		}
		else {
			return 1;
		}
	}
}