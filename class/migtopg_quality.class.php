<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
 *
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
 *  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
 *  \file       htdocs/custom/migtopg/class/migtopg_quality.class.php
*  \ingroup    MigToPG
*  \brief      Class use to compare data between both database
*/

/**
 *	\class      DoliDBPgsql Migration to Postgresql extansion
 *	\brief       Class use to compare data between both database
*/
class MigToPGQuality
{
	var $mysql_db;
	var $mysql_structure;
	var $postgresql_db;
	var $array_types_field;
	var $array_type_field; //Type of field to test
	var $persent_records; // percent of record to test
	var $error;
	var $errors;

	/**
	 *	Constructor.
	 *
	 *  @param		db		$mysql_db      		mysql database object
	 *  @param		db		$postgresql_db      postgresql database object
	 *  @param		object		$mysql_structure      MySQL structure database
	 *	@return	    int					1 if OK, 0 if not
	 */
	function __construct($mysql_db,$postgresql_db,$mysql_structure)
	{
		$this->mysql_db=$mysql_db;
		$this->mysql_structure=$mysql_structure;
		$this->postgresql_db = $postgresql_db;
		$this->array_types_field = array(0=>'All',1 =>'Caracters',2 =>'Date', 3 =>'Numbers');
		$errors = array();
		
		return 1;
	}
	
	
	/**
	 *	Return the diff between to database
	 * 
	 *	@return	    array					Array of diff
	 */
	function compareDb() {
		
		$arr_return = array();
		
		$obj_array_mysql=array();
		$obj_array_pgsql=array();
		
		$result=$this->mysql_structure->getListTables(true,false,false);
		
		if ($result>0) {
			foreach ($this->mysql_structure->mysql_tables as $table) {
				
				$sql_field_list='';
				
				foreach ($table->fields as $field) {
					
					if ($field->field_name=='virtual') {
						$sql_field_list.='`'.$field->field_name.'`'.',';
					} else {
						$sql_field_list.=$field->field_name.',';
					}
					
				}
				
				//Remove the last coma
				$sql_field_list=substr($sql_field_list, 0, -1);
			
				$sql='SELECT '.$sql_field_list.' FROM '.$table->table_name.' ORDER BY '.$sql_field_list;
				
				$ret_mysql=$this->mysql_db->query($sql);
				if (!$ret_mysql) {
					$this->error++;
					$this->errors[]=$this->mysql_db->lasterror();
					dol_syslog(get_class($this)."::compareDb sql_mysql=".$sql, LOG_ERROR);
				} else {
					$num_mysql = $this->mysql_db->num_rows($ret_mysql);
				}
				
				
				$sql=str_replace('`', '', $sql);
				
				$ret_pgsql=$this->postgresql_db->query($sql);
				if (!$ret_pgsql) {
					$this->error++;
					$this->errors[]=$this->postgresql_db->lasterror();
					dol_syslog(get_class($this)."::compareDb sql_pgsql=".$sql, LOG_ERROR);
				}
				else {
					$num_pgsql = $this->postgresql_db->num_rows($ret_pgsql);
				}

				$i = 0;
				if (($num_mysql==$num_pgsql) && !($this->error))
				{
					while( $i < $num_mysql)
					{
						$obj_mysql = $this->mysql_db->fetch_object($ret_mysql);
						$obj_pgsql = $this->postgresql_db->fetch_object($ret_pgsql);
						
						$obj_array_mysql=get_object_vars($obj_mysql);
						$obj_array_pgsql=get_object_vars($obj_pgsql);
						
						$diff_tmp=array_diff($obj_array_pgsql,$obj_array_mysql);
				
						if (count($diff_tmp)>0) {
							$arr_return[]=array(true,$table->table_name,$obj_array_pgsql,$obj_array_mysql,$diff_tmp);
						}
						$i++;
					}
				}else {
					$arr_return[]=array(false,$table->table_name);
				}
					
				$this->postgresql_db->free($ret_pgsql);
				$this->mysql_db->free($ret_mysql);
				
				
			}
		}
		
		if (!(empty($this->error))) {
			return -1;
		}else {
			return $arr_return;
		}
			
		
	}
	
}	