<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
 *  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
 *  \file       htdocs/custom/migtopg/lib/html.frommigtopg.class.php
 *  \ingroup    MigToPG
 *	\brief      File of class with all html predefined components
 */

$langs->load('migtopg@migtopg');

/**
 *	Class to manage generation of HTML components
 *	Only common components must be here.
 */
class FormMigToPG
{
	var $db;
	var $error;

	/**
	 * Constructor
	 *
	 * @param		DoliDB		$db      Database handler
	 */
	public function __construct($db)
	{
		$this->db = $db;
	}

	/**
	 *  Return HTML control for edit or display variable
	 *
	 *  @param	string	$htmlname		name of the HTML control
	 *  @param	string	$typecontrol	text or password
	 *  @param	string	$value 	 		value to display
	 *  @param	int		$size			size of the input control
	 *  @param	string	$mode			edit or view
	 *  @return string      		 HTML control
	*/
	function inputControl($htmlname,$typecontrol,$size,$value,$mode)
	{
		$html_control='';
		
		if ($mode == 'view') {
			if ($typecontrol=='password') {
				$html_control=preg_replace('/./i','*',$value); 
			}
			else {			
				$html_control= $value;
			}
		}
		elseif ($mode == 'edit') {
			$html_control='<input type="'.$typecontrol.'" name="'.$htmlname.'" value="'.$value.'" size="'.$size.'" >';
		}
		
		return $html_control;
	}
	
	/**
	 *  Return HTML control for select collation and ctype
	 *
	 *  @param	string	$htmlname		name of the HTML control
	 *  @param	string	$selected 	 	default selected value
	 *  @return string      		 HTML control
	 */
	function selectCollationTargetDB($htmlname,$selected)
	{
		$html_control='<select id="'.$htmlname.'" name="'.$htmlname.'">';
		$this->db->getListCollationAvalaible();
		if (count($this->db->line_dbcolation)>0) {
			foreach($this->db->line_dbcolation as $linecollation) {
				if (!empty($selected) && $selected == $linecollation) {
					$html_control.= '<option value="'.$linecollation.'" selected="selected">'.$linecollation.'</option>';
				}
				else {
					$html_control.= '<option value="'.$linecollation.'">'.$linecollation.'</option>';
				}
			}
		}
		$html_control.='</select>';
		return $html_control;
	}
	
	/**
	 *  Return HTML control for select charset
	 *
	 *  @param	string	$htmlname		name of the HTML control
	 *  @param	string	$selected 	 	default selected value
	 *  @return string      		 HTML control
	 */
	function selectCharsetTargetDB($htmlname,$selected)
	{
		$html_control='<select id="'.$htmlname.'" name="'.$htmlname.'">';
		$this->db->getListCharsetAvalaible();
		if (count($this->db->line_dbcharset)>0) {
			foreach($this->db->line_dbcharset as $linecharset) {
				if (!empty($selected) && $selected == $linecollation) {
					$html_control.= '<option value="'.$linecharset.'" selected="selected">'.$linecharset.'</option>';
				}
				else {
					$html_control.= '<option value="'.$linecharset.'">'.$linecharset.'</option>';
				}
			}
		}
		$html_control.='</select>';
		return $html_control;
	}
	
	
}
