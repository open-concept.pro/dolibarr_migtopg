<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
 *
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
 *  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
 *  \file       htdocs/custom/migtopg/class/migtopg_tbl.class.php
*  \ingroup    MigToPG
*  \brief      Class use to store tables structures
*/

/**
 *	\class      DoliDBPgsql Migration to Postgresql extansion
 *	\brief      Class to store table information
*/
class MigToPGTables 
{
	var $table_name;
	var $fields;
	var $constraints;
	var $indexs;

	/**
	 *	Constructor.
	 *
	 *  @param		string		$table_name      table names
	 *	@return	    int					1 if OK, 0 if not
	 */
	function __construct($table_name)
	{
		$this->table_name=$table_name;
		$this->fields = array();
		$this->constraints = array();
		$this->indexs = array();
		
		return 1;
	}
	
}	

/**
 *	\class      DoliDBPgsql Migration to Postgresql extansion
 *	\brief      Class to store field information
 */
class MigToPGField
{
	var $field_name;
	var $type; // datatype field
	var $can_be_null; //Is nullable
	var $default_value; //dafault value
	var $is_PK; // Is primary key
	var $is_auto_update_CTS; // Is auto update current time stamp
	var $is_autoincrement; // Is auto increment
	var $comment; //some column can have comment

	/**
	 *	Constructor.
	 *
	 *  @param		string		$table_name      table names
	 *	@return	    int					1 if OK, 0 if not
	 */
	function __construct($field_name)
	{
		$this->field_name=$field_name;
		$this->can_be_null=false;
		$this->is_PK=false;
		$this->is_auto_update_CTS=false;
		$this->is_autoincrement=false;
		
		return 1;
	}

}

/**
 *	\class      DoliDBPgsql Migration to Postgresql extansion
 *	\brief      Class to store constraint
 */
class MigToPGConstraint
{
	var $constraint_name;
	var $types;
	var $type;
	var $fields;
	var $fields_target;
	var $on_clauses;
	var $on_UPDATE;
	var $on_DELETE;

	/**
	 *	Constructor.
	 *
	 *  @param		string		$constraint_name      constraint name
	 *	@return	    int					1 if OK, 0 if not
	 */
	function __construct($constraint_name)
	{
		$this->constraint_name=$constraint_name;
		$this->fields=array();
		$this->fields_target = array();
		$this->types=array(0=>'PRIMARY',1 =>'user_key',2 =>'foreign_key');
		$this->on_clauses=array(0=>'NO ACTION',1 =>'RESTRICT',2 =>'CASCADE', 3 =>'SET NULL');
		$this->on_UPDATE='';
		$this->on_DELETE='';

		return 1;
	}

}

/**
 *	\class      DoliDBPgsql Migration to Postgresql extansion
 *	\brief      Class to store index not unique of fk
 */
class MigToPGIndex
{
	var $index_name;
	var $fields;


	/**
	 *	Constructor.
	 *
	 *  @param		string		$index_name      index name
	 *	@return	    int					1 if OK, 0 if not
	 */
	function __construct($index_name)
	{
		$this->index_name=$index_name;
		$this->fields=array();

		return 1;
	}

}

/**
 *	\class      DoliDBPgsql Migration to Postgresql extansion
 *	\brief      Class to store duplication index name
 */
class MigToPGDuplicateIndexName
{
	var $table_name;
	var $type;
	var $old_name;
	var $new_name;


	/**
	 *	Constructor.
	 *
	 *  @param		string		$table_name      table name
	 *  @param		string		$old_name      old index name
	 *  @param		string		$new_name      new index name
	 *	@return	    int					1 if OK, 0 if not
	 */
	function __construct($table_name,$old_name,$new_name,$type)
	{
		$this->table_name=$table_name;
		$this->old_name=$old_name;
		$this->new_name=$new_name;
		$this->type=$type;

		return 1;
	}

}