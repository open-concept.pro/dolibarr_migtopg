<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
 *
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
 *  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
 *  \file       htdocs/custom/migtopg/exec/migtopg_step1.php
*  \ingroup    MigToPG
*  \brief      First Step of the migration script
*/

$res=@include("../../main.inc.php");				// For root directory
if (! $res) $res=@include("../../../main.inc.php");	// For "custom" directory
if (! $res) die("Include of main fails");

$res=dol_include_once('/migtopg/class/migtopg.class.php');
if (! $res) die("Include of class migtopg fails");
$res=dol_include_once('/core/lib/admin.lib.php');
if (! $res) die("Include of admin fails");

$langs->load("errors");
$langs->load("install");
$langs->load('migtopg@migtopg');

if (!$user->rights->migtopg->execmig) accessforbidden();

$error = 0;
$mesg_err = array();

$connect_server=false;
$dbexists=false;
$dbexists_withtbl=0;
$user_role_credb=0;
$user_role_owner=0;
$connexion_admin=0;
$conf_file_ok=0;
$continue_migration=0;
$version_postgresql_test=0;
$conf_dir_doc_ok=0;

$action = GETPOST('action','alpha');
$confirm = GETPOST('confirm','alpha');
$action_ori=$action;

$action='check';

$conffile = dol_buildpath('/conf/conf.php',0);

//Create own database object 
$MigToPg_db = new MigToPG();

$MigToPg_db->pg_host=$conf->global->MIG_TO_PG_DB_PG_SERVER;
$MigToPg_db->pg_port=$conf->global->MIG_TO_PG_DB_PG_PORT;
$MigToPg_db->pg_user=$conf->global->MIG_TO_PG_DB_USER;
$MigToPg_db->pg_password=$conf->global->MIG_TO_PG_DB_PASS;
$MigToPg_db->pg_db_name=$conf->global->MIG_TO_PG_DB_NAME;
$MigToPg_db->pg_root_user=$conf->global->MIG_TO_PG_DB_RUSER;
$MigToPg_db->pg_root_password=$conf->global->MIG_TO_PG_DB_RPASS;

//Drop existing database
if (($action_ori=='dropdb_confirm') && ($confirm=='yes')) {
	$ret = $MigToPg_db->dropDatabase();
	if ($ret<0) {
		$mesg_err[]=$MigToPg_db->error;
		$error++;
	}
}

if ($action=='check') {
	// Test if we conf file exists
	
	if ($conffile) {
		// Test if we can write conf file
		if (! is_writable($conffile))
		{
			$mesg_err[]=$langs->trans("ConfFileIsNotWritable",$conffile);
			$error++;
		}
		else {
			$conf_file_ok=1;
		}
	}
	
	// Test if we document directory
	if ($conf->migtopg->dir_output) {
		// Test if we can write conf file
		if (! is_writable($conf->migtopg->dir_output))
		{
			$mesg_err[]=$langs->trans("MigToPGDirIsNotWritable",$conf->migtopg->dir_output);
			$error++;
		}
		else {
			$conf_dir_doc_ok=1;
		}
	}
	
	//Test connexion to server without database name. 
	$ret = $MigToPg_db->testConnexion('server');
	if ($ret<0) {
		$mesg_err[] = $MigToPg_db->error;
		$error++;
	}else {
		$connect_server=true;
		
		//test if user connexion get role to createdb
		$ret = $MigToPg_db->isUserCreatedbRole();
		if ($ret>0) {
			$user_role_credb=$MigToPg_db->user_role_credb;
		}
		else {
			$mesg_err[]=$MigToPg_db->error;
			$error++;
		}

		//Test version server
		$version_postgresql_test=versioncompare($MigToPg_db->getVersionArray(),$MigToPg_db->versionmin);
		$version_postgresql=$MigToPg_db->getVersion();
		
		$version_mysql_test=versioncompare($db->getVersionArray(),array(5,0,0));
		$version_mysql_test=$db->getVersion();
		
		//Close connexino to DB
		$ret = $MigToPg_db->close();
		if (!$ret) {
			$mesg_err[] =$langs->trans("MigToPGDisconnectErr",$MigToPg_db->pg_host);
			$error++;
		}
	}
	
	//Test connexion to server with admin right.
	$ret = $MigToPg_db->testConnexion('admin');
	if ($ret>0) {
		//test if admin user connexion get role to createdb
		$ret = $MigToPg_db->isUserCreatedbRole('admin');
		if ($ret>0) {
			$connexion_admin=1;
		}
		else {
			$mesg_err[]=$MigToPg_db->error;
			$error++;
		}
		$connect_server=true;
		
		//Close connexino to DB
		$ret = $MigToPg_db->close();
		if (!$ret) {
			$mesg_err[] =$langs->trans("MigToPGDisconnectErr",$MigToPg_db->pg_host);
			$error++;
		}
	}
	
	if ((!$error) || (($action_ori=='dropdb_confirm'))) {
		//Test connexion to server with database name. 
		$ret = $MigToPg_db->testConnexion('database');
		if ($ret>0) {
			
			$dbexists=true;
					
			//test if user connexion is db owner
			$ret = $MigToPg_db->isUserIsDbowner();
			if ($ret>0) {
				$user_role_owner=$MigToPg_db->user_role_owner;
			}
			else {
				$mesg_err[]=$MigToPg_db->error;
				$error++;
			}
			
			//determine if table with dolibarr prefix table exists inside
			$ret = $MigToPg_db->isDolibarrTableExist();
			if ($ret>=0) {
				$dbexists_withtbl=$MigToPg_db->dbexists_withtbl;
			}
			else {
				$mesg_err[]=$MigToPg_db->error;
				$error++;
			}
			
			//Close connexino to DB
			$ret = $MigToPg_db->close();
			if (!$ret) {
				$mesg_err[] =$langs->trans("MigToPGDisconnectErr",$MigToPg_db->pg_host);
				$error++;
			}
		}
	}
	
}

/*
 *  Page
*
*/

llxHeader();

$form=new Form($db);

// Admin var of module
print_titre($langs->trans("MigToPGCheckSummary"));

/*
 * Confirmation de la suppression
*/
if ($action_ori == 'dropdb')
{
	//the resp=dropdb is mandatory but not use at all. we can write toto=toto it will be the same, it's because form_confirm method
	//always generate url confirm with syntax '&action=...' 
	$ret=$form->form_confirm($_SERVER['PHP_SELF'].'?resp=dropdb',$langs->trans("MigToPGDropDB").'?',$langs->trans("MigToPGQDropDBHelp"),"dropdb_confirm",'','',1);
	if ($ret == 'html') print '<br>';
}

if ($error) {
	setEventMessages('',$mesg_err,'errors');
}

print '<table class="noborder" width="100%">';

print '<tr class="liste_titre">';
print '<td width="30%">'.$langs->trans("MigToPGCheck").'</td>';
print '<td width="5%">'.$langs->trans("MigToPGResult").'</td>';
print '<td align="left">'.$langs->trans("MigToPGComment").'</td>';
print "</tr>\n";


//Fichier de configuration
print '<tr class="pair"><td>'.$langs->trans("ConfigurationFile").'</td>';
print '<td align="left">';
if ($conf_file_ok) {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/tick.png',1).'" alt="Ok">';
}else {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="Ko">';
}
print '</td><td align="left">';
if (!$conf_file_ok) {
	print $langs->trans("CorrectProblemAndReloadPage",$_SERVER['PHP_SELF'].'?action=check');
	$continue_migration=0;
}
print '</td>';
print '</tr>';

//Répertoire de docu
print '<tr class="pair"><td>'.$langs->trans("MigToPGDirDoc").'</td>';
print '<td align="left">';
if ($conf_dir_doc_ok) {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/tick.png',1).'" alt="Ok">';
}else {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="Ko">';
}
print '</td><td align="left">';
if (!$conf_dir_doc_ok) {
	print $langs->trans("CorrectProblemAndReloadPage",$_SERVER['PHP_SELF'].'?action=check');
	$continue_migration=0;
}
print '</td>';
print '</tr>';


//Serveur
print '<tr class="impair"><td>'.$langs->trans("MigToPGServer").'</td>';
print '<td align="left">';
if ($connect_server) {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/tick.png',1).'" alt="Ok">';
}else {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="Ko">';
}
print '</td><td align="left">';
if (!$connect_server) {
	print $langs->trans("MigToPGInstallPostgres");
	$continue_migration=0;
}
print '</td>';
print '</tr>';

//Version PostgreSQL
print '<tr class="pair"><td>'.$langs->trans("MigToPGVersionPostgres").'</td>';
print '<td align="left">';
if ($version_postgresql_test>=0) {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/tick.png',1).'" alt="Ok">';
}elseif (($version_postgresql_test==-2) || ($version_postgresql_test==-3)) {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/warning.png',1).'" alt="Ko">';
}elseif ($version_postgresql_test<0) {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="Ko">';
}
print '</td><td align="left">';
if ($version_postgresql_test<0) {
	print $langs->trans("MigToPGVersionPostgresHelp",$version_postgresql);
}
print '</td>';
print '</tr>';

//Version MySQL
print '<tr class="pair"><td>'.$langs->trans("MigToPGVersionMysql").'</td>';
print '<td align="left">';
if ($version_mysql_test>=0) {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/tick.png',1).'" alt="Ok">';
}elseif ($version_mysql_test<0) {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="Ko">';
}
print '</td><td align="left">';
if ($version_postgresql_test<0) {
	print $langs->trans("MigToPGVersionMysqlHelp",$version_mysql);
}
print '</td>';
print '</tr>';
 
//Base de donnée
if (!$dbexists) {
	if ((!$conf_file_ok) || (!$conf_dir_doc_ok)) {
		$txt_res_action=$langs->trans("CorrectProblemAndReloadPage");
	} else {
		$txt_res_action=$langs->trans("MigToPGDBNotExist");
		if ($user_role_credb) {
			$img_res_action='<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/warning.png',1).'" alt="ko">';
			$txt_res_action.='<table><tr><td>'.$langs->trans("MigToPGQCreateDB");
			$txt_res_action.='</td><td><a class="butAction" href="'.dol_buildpath('/migtopg/exec/migtopg_createdb.php',1).'">'.$langs->trans("MigToPGCreateDB").'</a></td></tr></table>';
			$continue_migration=0;
		}
		else {
			if ($connexion_admin) {
				$img_res_action='<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/warning.png',1).'" alt="ko">';
				$txt_res_action.='<table><tr><td>'.$langs->trans("MigToPGQCreateDB");
				$txt_res_action.='</td><td><a class="butAction" href="'.dol_buildpath('/migtopg/exec/migtopg_createdb.php',1).'">'.$langs->trans("MigToPGCreateDB").'</a></td></tr></table>';
				$continue_migration=0;
			}else {
				$img_res_action='<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="ko">';
				$txt_res_action.= '<BR>'.$langs->trans("MigToPGCannotCreDB");
				$txt_res_action.= '<BR>'.$langs->trans("MigToPGNoAdminConnect");
				$txt_res_action.= '<BR>'.$langs->trans("MigToPGInstallPostgres");
				$continue_migration=0;
			}
		}
	}
}
if ($dbexists_withtbl) {
	$txt_res_action=$langs->trans("MigToPGTTblExists",'<b>'.MAIN_DB_PREFIX.'</b>');
	if ($connexion_admin) {
		$img_res_action='<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/warning.png',1).'" alt="ko">';
		$txt_res_action.='<table><tr><td>'.$langs->trans("MigToPGQDropDB");
		$txt_res_action.='</td><td><a class="butAction" href="'.$_SERVER["PHP_SELF"].'?action=dropdb">'.$langs->trans("MigToPGDropDB").'</a></td></tr></table>';
		$continue_migration=0;
	} else {
		$img_res_action='<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="ko">';
		$txt_res_action.= '<BR>'.$langs->trans("MigToPGCannotDelDBCred");
		$txt_res_action.= '<BR>'.$langs->trans("MigToPGNoAdminConnect");
		$continue_migration=0;
	}
}
if ($dbexists && $user_role_owner && !$dbexists_withtbl) {
	$img_res_action='<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/tick.png',1).'" alt="Ok">';
	$continue_migration=1;
}else {
	$img_res_action='<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="Ko">';
}
print '<tr class="impair"><td>'.$langs->trans("MigToPGDatabase").'</td>';
print '<td align="left">';
print $img_res_action;
print '</td><td align="left">';
print $txt_res_action;
print '</td>';
print '</tr>';

print '</table>';

// Boutons actions
print '<div class="tabsAction">';

print '<a class="butAction" href="'.dol_buildpath('/migtopg/admin/adm_migtopg.php',1).'">'.$langs->trans("Modify").'</a>';

if ($user->rights->migtopg->execmig && $continue_migration)
{
	print '<a class="butAction" href="'.dol_buildpath('/migtopg/exec/migtopg_step2.php',1).'">'.$langs->trans("MigToPGExecuteStep2").'</a>';
}
else
{
	print '<a class="butActionRefused" href="#" title="'.$langs->trans("MigToPGExecuteStep2").'">'.$langs->trans("MigToPGExecuteStep2").'</a>';
}

print '</div>';

$db->close();

llxFooter();