<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
 *
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
 *  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
 *  \file       htdocs/custom/migtopg/exec/migtopg_step4.php
*  \ingroup    MigToPG
*  \brief      4th Step of the migration script
*/

$res=@include("../../main.inc.php");				// For root directory
if (! $res) $res=@include("../../../main.inc.php");	// For "custom" directory
if (! $res) die("Include of main fails");

dol_include_once('/migtopg/class/migtopg.class.php');
dol_include_once('/migtopg/class/mysql_structures.class.php');

$langs->load('migtopg@migtopg');

if (!$user->rights->migtopg->execmig) accessforbidden();

$action = GETPOST('action','alpha');

$error = 0;
$mesg_err = array();

//Create own database object
$MigToPg_db = new MigToPG();

$MigToPg_db->pg_host=$conf->global->MIG_TO_PG_DB_PG_SERVER;
$MigToPg_db->pg_port=$conf->global->MIG_TO_PG_DB_PG_PORT;
$MigToPg_db->pg_user=$conf->global->MIG_TO_PG_DB_USER;
$MigToPg_db->pg_password=$conf->global->MIG_TO_PG_DB_PASS;
$MigToPg_db->pg_db_name=$conf->global->MIG_TO_PG_DB_NAME;
$MigToPg_db->pg_root_user=$conf->global->MIG_TO_PG_DB_RUSER;
$MigToPg_db->pg_root_password=$conf->global->MIG_TO_PG_DB_RPASS;

$ret=$MigToPg_db->connectPgSQL($MigToPg_db->pg_host, $MigToPg_db->pg_user, $MigToPg_db->pg_password, $MigToPg_db->pg_db_name, $MigToPg_db->pg_port);
if ($ret<0) {
	$mesg_err[] = $MigToPg_db->error;
	$error++;
}

/*
 *  Page
*
*/

llxHeader();

$form=new Form($db);

// Admin var of module
print_titre($langs->trans("MigToPGExecuteStep4"));

if ($error) {
	dol_htmloutput_errors('',$mesg_err);
}

if ($action=='copydata') {
	print '<table width="100%" class="border">';
	print '<tr class="liste_titre">';
	print '<td width="20%">'.$langs->trans("MigToPGTable").'</td>';
	print '<td width="5%">'.$langs->trans("MigToPGResult").'</td>';
	print '<td align="left">'.$langs->trans("MigToPGComment").'</td>';
	print '<td align="left">'.$langs->trans("MigToPGFile").'</td>';
	print "</tr>\n";
	
	//get number of tables in sources
	$mysql_structure = new MigToPG_MySQLStructure($db);
	$nb_tbl_source=$mysql_structure->getListTables(true,false,false);
	
	$style='impair';
	foreach ($mysql_structure->mysql_tables as $tables) {
		if ($style=='pair') {$style='impair';}
		else {$style='pair';}
		
		print '<tr class="'.$style.'">';
		print '<td>'.$tables->table_name.'</td>'."\n";
	
		$nb_records=$mysql_structure->generateInsertScript($tables,$MigToPg_db);
		
		$sql_insert=$mysql_structure->sql_insert;
		
		$file_script='';
		
		if ($nb_records!=-1) {
			
			if ($nb_records!=0) {
				
				// Definition of $dir and $file
				$dir = $conf->migtopg->dir_output;
				$file = $dir.'/'.'tbl_insert_'.$tables->table_name.'.sql';
				
				if (file_exists($dir))
				{
					if (file_exists($file)) {unlink($file);}
					$file=fopen($file,"a+");
					if ($file!==false) {
						fwrite($file,$sql_insert."\n");
						fclose($file);
						$file_script='<a href="'.dol_buildpath('/document.php',1).'?modulepart=migtopg&file=tbl_insert_'.$tables->table_name.'.sql" alt="'.$langs->trans("MigToPGInsertFile").'" title="'.$langs->trans("MigToPGInsertFile").'">';
						$file_script.='<img src="'.dol_buildpath('/migtopg/img/script_file.png',1).'" border="0" align="absmiddle" hspace="2px" ></a>';
					}else {
						$file_script=$langs->trans("MigToPGInsertFileErr").':'.$langs->trans("MigToPGSavInsertFileErr",$dir);
						$error++;
					}
				}
				
				$ret =$MigToPg_db->query($sql_insert,0,'ddl');
				
				if (!$ret) {
					$status='<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="Ko">'."\n";
					$comment=$MigToPg_db->lasterror();
					$error++;
				}else{
					if ($MigToPg_db->affected_rows($ret)!=$nb_records) {
						$status='<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/warning.png',1).'" alt="Ok">';
						$comment=$langs->trans("MigToPGNbLineDiff").'<BR>';
						$comment.=$langs->trans("MigToPGNbLineSource",$nb_records).'<BR>';
						$comment.=$langs->trans("MigToPGNbDataInsert",$MigToPg_db->affected_rows($ret));
					}else {
						$status='<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/tick.png',1).'" alt="Ok">';
						$comment=$langs->trans("MigToPGNbLineSource",$nb_records).'<BR>';
						$comment.=$langs->trans("MigToPGNbDataInsert",$MigToPg_db->affected_rows($ret));
					}
				}
			}else {
				$status='<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/tick.png',1).'" alt="Ok">';
				$comment=$langs->trans("MigToPGNoDataToInsert");
			}
		}else {
			$status='<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="Ko">';
			$comment=$mysql_structure->error;
			$error++;
		}
	
		print '<td>'.$status.'</td>'."\n";
		print '<td>'.$comment.'</td>'."\n";
		print '<td>'.$file_script.'</td>'."\n";
	
		print "</tr>\n";
		
		//Flush outpur on each tables
		flush();
	}
	
	print "</table>\n";
}


// Boutons actions
print '<div class="tabsAction">';

if ($user->rights->migtopg->execmig && !$error)
{
	print '<a class="butAction" href="'.dol_buildpath('/migtopg/exec/migtopg_step5.php',1).'?action=execscript">'.$langs->trans("MigToPGExecuteStep5").'</a>';
}
else
{
	print '<a class="butActionRefused" href="#" title="'.$langs->trans("MigToPGExecuteStep5").'">'.$langs->trans("MigToPGExecuteStep5").'</a>';
}

print '</div>';

$db->close();

llxFooter();