<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
 *
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
 *  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
 *  \file       htdocs/custom/migtopg/exec/migtopg_step8.php
*  \ingroup    MigToPG
*  \brief      8th Step of the migration script
*/

$res=@include("../../main.inc.php");				// For root directory
if (! $res) $res=@include("../../../main.inc.php");	// For "custom" directory
if (! $res) die("Include of main fails");

dol_include_once('/migtopg/class/mysql_structures.class.php');
dol_include_once('/migtopg/class/migtopg.class.php');
dol_include_once('/migtopg/class/migtopg_quality.class.php');

$langs->load('migtopg@migtopg');

if (!$user->rights->migtopg->execmig) accessforbidden();

$action = GETPOST('action','alpha');

$error = 0;
$mesg_err = array();

//Create own database object
$MigToPg_db = new MigToPG();

$MigToPg_db->pg_host=$conf->global->MIG_TO_PG_DB_PG_SERVER;
$MigToPg_db->pg_port=$conf->global->MIG_TO_PG_DB_PG_PORT;
$MigToPg_db->pg_user=$conf->global->MIG_TO_PG_DB_USER;
$MigToPg_db->pg_password=$conf->global->MIG_TO_PG_DB_PASS;
$MigToPg_db->pg_db_name=$conf->global->MIG_TO_PG_DB_NAME;
$MigToPg_db->pg_root_user=$conf->global->MIG_TO_PG_DB_RUSER;
$MigToPg_db->pg_root_password=$conf->global->MIG_TO_PG_DB_RPASS;

$ret=$MigToPg_db->connectPgSQL($MigToPg_db->pg_host, $MigToPg_db->pg_user, $MigToPg_db->pg_password, $MigToPg_db->pg_db_name, $MigToPg_db->pg_port);
if ($ret<0) {
	$mesg_err[] = $MigToPg_db->error;
	$error++;
}

if ((!$error) && ($action=='check')) {
	//Contruct current table structure on Mysql
	$mysql_structure = new MigToPG_MySQLStructure($db);
	
	$db_quality=new MigToPGQuality($db,$MigToPg_db,$mysql_structure);
	$db_diff=$db_quality->compareDb();
	if ($db_diff==-1) {
		$mesg_err = $db_quality->errors;
		$error++;
	}
		
}else {
	$mesg_err[]=$langs->trans("MigToPGSpecialFileErr").':'.$mysql_structure->error;
	$error++;
}

/*
 *  Page
*
*/

llxHeader();

$form=new Form($db);

// Admin var of module
print_titre($langs->trans("MigToPGExecuteStep8"));

if ($error) {
	dol_htmloutput_errors('',$mesg_err,1);
}
if (count($db_diff)>0) {
	print '<table width="100%" class="border">';
	print '<tr class="liste_titre">';
	print '<td width="20%">'.$langs->trans("MigToPGTable").'</td>';
	print '<td align="left">'.$langs->trans("MigToPGComment").'</td>';
	print "</tr>\n";
	
	foreach ($db_diff as $line) {
		print '<tr>';
		print '<td>'.$line[1].'</td>';
		if ($line[0]) {
			print '<td>';
			//Postgres ligne
			print $langs->trans("MigToPGDiffInBold");
			print '<br>';
			print $langs->trans("MigToPGLinePG");
			print '<table><tr class="liste_titre">';
			foreach ($line[2] as $key_pg => $line_pg) {
				print '<td>'.$key_pg.'</td>';
			}
			print '</tr><tr>';
			foreach ($line[2] as $key_pg => $line_pg) {
				if (strlen($line_pg)>100){
					$line_txt=substr($line_pg,0,100).'...';
				}else {
					$line_txt=$line_pg;
				}
				
				if (key($line[4])==$key_pg) {
					print '<td><b>'.$line_txt.'</b></td>';
				} else {
					print '<td>'.$line_txt.'</td>';
				}
			}
			print '</tr></table>';
			
			print '<br>';
			
			//MySQL ligne
			print $langs->trans("MigToPGLineMysql");
			print '<table><tr class="liste_titre">';
			foreach ($line[3] as $key_mysql => $line_mysql) {
				print '<td>'.$key_mysql.'</td>';
			}
			print '</tr><tr>';
			foreach ($line[3] as $key_mysql => $line_mysql) {
				if (strlen($line_mysql)>100){
					$line_txt=substr($line_mysql,0,100).'...';
				}else {
					$line_txt=$line_mysql;
				}
					
				
				if (key($line[4])==$key_mysql) {
					print '<td><b>'.$line_txt.'</b></td>';
				} else {
					print '<td>'.$line_txt.'</td>';
				}
			}
			print '</tr></table>';
			
			print '</td>';
		}else{
			print '<td>'.$langs->trans("MigToPGNbLineDiff").'</td>';
		}
			
		
		print '</tr>';
	}
	
	print '</table>';
}else {
	print $langs->trans("MigToPGNoDiff");
}

// Boutons actions
print '<div class="tabsAction">';

if ($user->rights->migtopg->execmig && !$error)
{
	print '<a class="butAction" href="'.dol_buildpath('/migtopg/exec/migtopg_step9.php',1).'?action=check">'.$langs->trans("MigToPGExecuteStep9").'</a>';
}
else
{
	print '<a class="butActionRefused" href="#" title="'.$langs->trans("MigToPGExecuteStep9").'">'.$langs->trans("MigToPGExecuteStep9").'</a>';
}

print '</div>';

$db->close();

llxFooter();