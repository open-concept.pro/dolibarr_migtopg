<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
 *
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
 *  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
 *  \file       htdocs/custom/migtopg/exec/migtopg_step9.php
*  \ingroup    MigToPG
*  \brief      9th Step of the migration script
*/

/*error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('html_errors', false);*/

$res=@include("../../main.inc.php");				// For root directory
if (! $res) $res=@include("../../../main.inc.php");	// For "custom" directory
if (! $res) die("Include of main fails");

dol_include_once('/core/lib/files.lib.php');

$langs->load("errors");
$langs->load("install");
$langs->load('migtopg@migtopg');

if (!$user->rights->migtopg->execmig) accessforbidden();

$action = GETPOST('action','alpha');

$error = 0;
$mesg_err = array();
$mesg='';

$conffile = dol_buildpath('/conf/conf.php',0);
$conf_file_ok=0;

if ($action=='check') {
	// Test if we conf file exists
	if ($conffile) {
		// Test if we can write conf file
		if (! is_writable($conffile))
		{
			$mesg_err[]=$langs->trans("ConfFileIsNotWritable",$conffile);
			$error++;
		}
		else {
			$conf_file_ok=1;
		}
	}
}elseif ($action=='updateconf') {
	// Save old conf file on disk
	if (file_exists($conffile))
	{
		// We must ignore errors as an existing old file may already exists and not be replacable or
		// the installer (like for ubuntu) may not have permission to create another file than conf.php.
		// Also no other process must be able to read file or we expose the new file, so content with password.
		
		@dol_copy($conffile, $conffile.'.old_db','0777');
	}
	
	$buffer=file_get_contents($conffile);
	if ($buffer!==false) {

		$buffer=str_replace('$dolibarr_main_db_host=\''.$conf->db->host.'\';','$dolibarr_main_db_host=\''.$conf->global->MIG_TO_PG_DB_PG_SERVER.'\';',$buffer);
		$buffer=str_replace('$dolibarr_main_db_port=\''.$conf->db->port.'\';','$dolibarr_main_db_port=\''.$conf->global->MIG_TO_PG_DB_PG_PORT.'\';',$buffer);
		$buffer=str_replace('$dolibarr_main_db_name=\''.$conf->db->name.'\';','$dolibarr_main_db_name=\''.$conf->global->MIG_TO_PG_DB_NAME.'\';',$buffer);
		$buffer=str_replace('$dolibarr_main_db_user=\''.$conf->db->user.'\';','$dolibarr_main_db_user=\''.$conf->global->MIG_TO_PG_DB_USER.'\';',$buffer);
		$buffer=str_replace('$dolibarr_main_db_pass=\''.$conf->db->pass.'\';','$dolibarr_main_db_pass=\''.$conf->global->MIG_TO_PG_DB_PASS.'\';',$buffer);
		$buffer=str_replace('$dolibarr_main_db_type=\''.$conf->db->type.'\';','$dolibarr_main_db_type=\'pgsql\';',$buffer);
	
		$ret=file_put_contents($conffile,$buffer);
		
		if ($ret===false){
			$mesg_err[]=$langs->trans("ConfFileIsNotWritable",$conffile);
			$error++;
		}
	}else {
		$mesg_err[]=$langs->trans("ConfFileIsNotWritable",$conffile);
		$error++;
	}
	
	
}

/*
 *  Page
*
*/

llxHeader();

$form=new Form($db);

// Admin var of module
if ($action=='check') {
	$title = $langs->trans("MigToPGExecuteStep9");
}elseif ($action=='updateconf') {
	$title = $langs->trans("MigToPGExecuteStep9Update");
}
	 
print_titre($title);

if ($error) {
	dol_htmloutput_errors('',$mesg_err,1);
}

if ($action=='check') {
	print '<table class="noborder" width="100%">';
	print '<tr class="liste_titre">';
	print '<td width="30%">'.$langs->trans("MigToPGCheck").'</td>';
	print '<td width="5%">'.$langs->trans("MigToPGResult").'</td>';
	print '<td align="left">'.$langs->trans("MigToPGComment").'</td>';
	print "</tr>\n";
	
	//Fichier de configuration
	print '<tr class="pair"><td>'.$langs->trans("ConfigurationFile").'</td>';
	print '<td align="left">';
	if ($conf_file_ok) {
		print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/tick.png',1).'" alt="Ok">';
	}else {
		print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="Ko">';
	}
	print '</td><td align="left">';
	if (!$conf_file_ok) {
		print $langs->trans("CorrectProblemAndReloadPage",$_SERVER['PHP_SELF'].'?action=check');
		$continue_migration=0;
	}
	print '</td>';
	print '</tr>';
	
	print '</table>';
	
	print '<BR>';
	
	//Valeur du ficher de configuration
	print '<table class="noborder" width="100%">';
	print '<tr class="liste_titre">';
	print '<td width="30%">'.$langs->trans("MigToPGKey").'</td>';
	print '<td width="5%">'.$langs->trans("MigToPGOldVal").'</td>';
	print '<td align="left">'.$langs->trans("MigToPGNewVal").'</td>';
	print "</tr>\n";
	
	print '<tr class="pair"><td>$dolibarr_main_db_host</td>';
	print '<td align="left">';
	print $conf->db->host;
	print '</td><td align="left">';
	print $conf->global->MIG_TO_PG_DB_PG_SERVER;
	print '</td>';
	print '</tr>';
	
	print '<tr class="impair"><td>$dolibarr_main_db_port</td>';
	print '<td align="left">';
	print $conf->db->port;
	print '</td><td align="left">';
	print $conf->global->MIG_TO_PG_DB_PG_PORT;
	print '</td>';
	print '</tr>';
	
	print '<tr class="pair"><td>$dolibarr_main_db_name</td>';
	print '<td align="left">';
	print $conf->db->name;
	print '</td><td align="left">';
	print $conf->global->MIG_TO_PG_DB_NAME;
	print '</td>';
	print '</tr>';
	
	print '<tr class="impair"><td>$dolibarr_main_db_user</td>';
	print '<td align="left">';
	print $conf->db->user;
	print '</td><td align="left">';
	print $conf->global->MIG_TO_PG_DB_USER;
	print '</td>';
	print '</tr>';
	
	print '<tr class="pair"><td>$dolibarr_main_db_pass</td>';
	print '<td align="left">';
	print $conf->db->pass;
	print '</td><td align="left">';
	print $conf->global->MIG_TO_PG_DB_PASS;
	print '</td>';
	print '</tr>';
	
	print '<tr class="impair"><td>$dolibarr_main_db_type</td>';
	print '<td align="left">';
	print $conf->db->type;
	print '</td><td align="left">';
	print 'pgsql';
	print '</td>';
	print '</tr>';

	print '</table>';
}elseif (($action=='updateconf') && (!$error)) {
	dol_htmloutput_mesg($langs->trans("MigToPGMigrationSuccess"));
}

// Boutons actions
print '<div class="tabsAction">';

if ($user->rights->migtopg->execmig && !$error)
{
	if ($action=='check') {
		print '<a class="butAction" href="'.dol_buildpath('/migtopg/exec/migtopg_step9.php',1).'?action=updateconf">'.$langs->trans("MigToPGExecuteStep9Update").'</a>';
	} elseif ($action=='updateconf') {
		print '<a class="butAction" href="'.dol_buildpath('/user/logout.php',1).'">'.$langs->trans("MigToPGLogout").'</a>';
	}
}
else
{
	if ($action=='check') {
		print '<a class="butActionRefused" href="#" title="'.$langs->trans("MigToPGExecuteStep9Update").'">'.$langs->trans("MigToPGExecuteStep9Update").'</a>';
	} elseif ($action=='updateconf') {
		print '<a class="butActionRefused" href="#" title="'.$langs->trans("MigToPGLogout").'">'.$langs->trans("MigToPGLogout").'</a>';
	}
	
}

print '</div>';

$db->close();

llxFooter();