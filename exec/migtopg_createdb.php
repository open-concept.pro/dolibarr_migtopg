<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
 *
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
 *  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
 *  \file       htdocs/custom/migtopg/exec/migtopg_etape1.php
*  \ingroup    MigToPG
*  \brief      First Step of the migration script
*/

$res=@include("../../main.inc.php");				// For root directory
if (! $res) $res=@include("../../../main.inc.php");	// For "custom" directory
if (! $res) die("Include of main fails");

dol_include_once('/migtopg/class/migtopg.class.php');
dol_include_once('/migtopg/class/html.formmigtopg.class.php');
dol_include_once('/core/lib/admin.lib.php');

$langs->load("errors");
$langs->load("install");
$langs->load("admin");
$langs->load('migtopg@migtopg');

if (!$user->rights->migtopg->execmig) accessforbidden();

$action = GETPOST('action','alpha');
if (empty($action)) {$action='view';}
$action_ori=$action;

$error = 0;
$mesg_err = array();
$db_creation_ok=false;

//Create own database object
$MigToPg_db = new MigToPG();

$MigToPg_db->pg_host=$conf->global->MIG_TO_PG_DB_PG_SERVER;
$MigToPg_db->pg_port=$conf->global->MIG_TO_PG_DB_PG_PORT;
$MigToPg_db->pg_user=$conf->global->MIG_TO_PG_DB_USER;
$MigToPg_db->pg_password=$conf->global->MIG_TO_PG_DB_PASS;
$MigToPg_db->pg_db_name=$conf->global->MIG_TO_PG_DB_NAME;
$MigToPg_db->pg_root_user=$conf->global->MIG_TO_PG_DB_RUSER;
$MigToPg_db->pg_root_password=$conf->global->MIG_TO_PG_DB_RPASS;

$formMigToPG=new FormMigToPG($MigToPg_db);

//First try connexion, you may have been redirect on this page by error
$ret = $MigToPg_db->testConnexion('database');
if ($ret>0) {
	$db_creation_ok=true;
}


if ($action == 'update' && empty($_POST["cancel"]))
{
	$pg_server_charset=GETPOST('MIG_TO_PG_DB_CHARSET','alpha');
	$res = dolibarr_set_const($db, 'MIG_TO_PG_DB_CHARSET', $pg_server_charset,'chaine',0,'',$conf->entity);
	if (! $res > 0) $error++;
	
	$pg_server_collation=GETPOST('MIG_TO_PG_DB_COLLATION','alpha');
	$res = dolibarr_set_const($db, 'MIG_TO_PG_DB_COLLATION', $pg_server_collation,'chaine',0,'',$conf->entity);
	if (! $res > 0) $error++;
	
	$pg_server_ctype=GETPOST('MIG_TO_PG_DB_CTYPE','alpha');
	$res = dolibarr_set_const($db, 'MIG_TO_PG_DB_CTYPE', $pg_server_ctype,'chaine',0,'',$conf->entity);
	if (! $res > 0) $error++;
	
	if (! $error)
	{
		$mesg = "<font class=\"ok\">".$langs->trans("SetupSaved")."</font>";
		$action='view';
	}
	else
	{
		$mesg_err[] = $langs->trans("Error");
		$action='edit';
	}
}else {
	$action = 'view';
}
	

if ($action_ori=='edit'){
	//Connect to PostgresSQL server without database to determine default charset collation and ctype value
	$ret=@$MigToPg_db->connectPgSQL($MigToPg_db->pg_host, $MigToPg_db->pg_user, $MigToPg_db->pg_password, '', $MigToPg_db->pg_port);
	if ($ret<0) {
		$mesg_err[] = $MigToPg_db->error;
		$error++;
	}
	if (!$error) {
		$MigToPg_db->forcecharset=$MigToPg_db->getDefaultCharacterSetDatabase();
		$MigToPg_db->forcecollate=$MigToPg_db->getDefaultCollationDatabase();
		$MigToPg_db->forcectype=$MigToPg_db->getDefaultCTypeDatabase();
	
		$html_selectCharset=$formMigToPG->selectCharsetTargetDB('MIG_TO_PG_DB_CHARSET', $MigToPg_db->forcecharset);
		$html_selectCollate=$formMigToPG->selectCollationTargetDB('MIG_TO_PG_DB_COLLATION', $MigToPg_db->forcecollate);
		$html_selectCType=$formMigToPG->selectCollationTargetDB('MIG_TO_PG_DB_CTYPE', $MigToPg_db->forcectype);
		
		//Close connexion to DB
		$ret = $MigToPg_db->close();
		if (!$ret) {
			$mesg_err[] =$langs->trans("MigToPGDisconnectErr",$MigToPg_db->pg_host);
			$error++;
		}
	}
}

if (($action_ori=='createdb') && !$db_creation_ok) {
	$ret=@$MigToPg_db->connectPgSQL($MigToPg_db->pg_host, $MigToPg_db->pg_user, $MigToPg_db->pg_password, '', $MigToPg_db->pg_port);
	if ($ret<0) {
		$mesg_err[] = $MigToPg_db->error;
		$error++;
	}
	if (!$error) {
		//Create database
		$ret=$MigToPg_db->migtopg_DDLCreateDb($MigToPg_db->pg_db_name, $conf->global->MIG_TO_PG_DB_CHARSET, $conf->global->MIG_TO_PG_DB_COLLATION, $conf->global->MIG_TO_PG_DB_CTYPE, $MigToPg_db->pg_user);
		if (empty($ret) || ($ret<0)) {
			$mesg_err[] = $langs->trans("MigToPGCreateDBError") .' '. $MigToPg_db->error;
			$error++;
		} else {
			$db_creation_ok=true;
			$mesg=$langs->trans("MigToPGCreateDBSucces");
		}
			
		
		//Close connexion to DB
		$ret = $MigToPg_db->close();
		if (!$ret) {
			$mesg_err[] =$langs->trans("MigToPGDisconnectErr",$MigToPg_db->pg_host);
			$error++;
		}
	}
}


/*
 *  Page
*
*/

llxHeader();

$form=new Form($db);

// Admin var of module
print_titre($langs->trans("MigToPGCreateDB"));

dol_htmloutput_mesg($mesg);

if ($error) {
	dol_htmloutput_errors('',$mesg_err,1);
}

print '<table class="noborder" width="100%">';

print '<tr class="liste_titre">';
print '<td width="20%">'.$langs->trans("Name").'</td>';
print '<td width="10%">'.$langs->trans("Valeur").'</td>';
print '<td align="left">'.$langs->trans("MigToPGComment").'</td>';
print "</tr>\n";

//Database name
print '<tr class="pair"><td>'.$langs->trans("DatabaseName").'</td>';
print '<td align="left">';
print $formMigToPG->inputControl('MIG_TO_PG_DB_NAME', 'text', 40, $MigToPg_db->pg_db_name, 'view');
print '</td><td align="left">';
print $form->textwithpicto('',$langs->trans("MigToPGDatabaseNameHelp"),1,'help');
print '</td>';
print '</tr>';

//Mode Edition
if ($action_ori=='edit') {
	print '<form method="post" action="'.$_SERVER['PHP_SELF'].'" enctype="multipart/form-data" >';
	print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
	print '<input type="hidden" name="action" value="update">';

	//Charset
	print '<tr class="impair"><td>'.$langs->trans("Charset").'</td>';
	print '<td align="left">';
	print $html_selectCharset;
	print '</td><td align="left">';
	print $langs->trans("MigToPGCharsetHelp",$db->getServerParametersValues('character_set_connection')['character_set_connection'],$MigToPg_db->forcecharset);
	print '</td>';
	print '</tr>';
	
	//collation
	print '<tr class="pair"><td>'.$langs->trans("Collation").'</td>';
	print '<td align="left">';
	print $html_selectCollate;
	print '</td><td align="left">';
	print $langs->trans("MigToPGCollationHelp",$db->getServerParametersValues('collation_connection')['collation_connection'],$MigToPg_db->forcecollate);
	print '</td>';
	print '</tr>';
	
	//CType
	print '<tr class="impair"><td>'.$langs->trans("Ctype").'</td>';
	print '<td align="left">';
	print $html_selectCType;
	print '</td><td align="left">';
	print $langs->trans("MigToPGCTypeHelp",$MigToPg_db->forcectype);
	print '</td>';
	print '</tr>';

}else {
	
	//Charset
	print '<tr class="impair"><td>'.$langs->trans("Charset").'</td>';
	print '<td align="left">';
	print $conf->global->MIG_TO_PG_DB_CHARSET;
	print '</td><td align="left">';
	if (empty($conf->global->MIG_TO_PG_DB_CHARSET)) {
		print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="ko">'.$langs->trans("MigToPGSetvar");
	}
	print '</td>';
	print '</tr>';
	
	//collation
	print '<tr class="pair"><td>'.$langs->trans("Collation").'</td>';
	print '<td align="left">';
	print $conf->global->MIG_TO_PG_DB_COLLATION;
	print '</td><td align="left">';
	if (empty($conf->global->MIG_TO_PG_DB_COLLATION)) {
		print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="ko">'.$langs->trans("MigToPGSetvar");
	}
	print '</td>';
	print '</tr>';
	
	//CType
	print '<tr class="impair"><td>'.$langs->trans("Ctype").'</td>';
	print '<td align="left">';
	print $conf->global->MIG_TO_PG_DB_CTYPE;
	print '</td><td align="left">';
	if (empty($conf->global->MIG_TO_PG_DB_CTYPE)) {
		print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="ko">'.$langs->trans("MigToPGSetvar");
	}
	print '</td>';
	print '</tr>';
}
	

print '</table>';

if ($action_ori=='edit') {
	
	print '<br><center>';
	print '<input class="button" type="submit" name="save" value="'.$langs->trans("Save").'">';
	print ' &nbsp; &nbsp; ';
	print '<input class="button" type="submit" name="cancel" value="'.$langs->trans("Cancel").'">';
	print '</center>';
	print '</form>';
	
}
elseif ($action=='view') {
	// Boutons actions
	print '<div class="tabsAction">';
	
	print '<a class="butAction" href="'.$_SERVER["PHP_SELF"].'?action=edit">'.$langs->trans("Modify").'</a>';
	
	if ((($user->rights->migtopg->execmig) 
		&& (!empty($conf->global->MIG_TO_PG_DB_CHARSET)) 
		&& (!empty($conf->global->MIG_TO_PG_DB_COLLATION))  
		&& (!empty($conf->global->MIG_TO_PG_DB_CTYPE)))
		&& (!$db_creation_ok)) {
		print '<a class="butAction" href="'.$_SERVER["PHP_SELF"].'?action=createdb">'.$langs->trans("MigToPGCreateDB").'</a>';
	}
	elseif ($db_creation_ok) {
		print '<a class="butActionRefused" href="#" title="'.$langs->trans("MigToPGCreateDB").'">'.$langs->trans("MigToPGCreateDB").'</a>';
	}else {
		print '<a class="butActionRefused" href="#" title="'.$langs->trans("MigToPGSetvar").'">'.$langs->trans("MigToPGSetvar").'</a>';
	}
	
	if ($user->rights->migtopg->execmig  && $db_creation_ok) {
		print '<a class="butAction" href="'.dol_buildpath('/migtopg/exec/migtopg_step1.php',1).'?action=check">'.$langs->trans("MigToPGExecuteStep1").'</a>';
	}else {
		print '<a class="butActionRefused" href="#" title="'.$langs->trans("MigToPGExecuteStep1").'">'.$langs->trans("MigToPGExecuteStep1").'</a>';
	}
	
	print '</div>';
}

print '</div>';


$db->close();


llxFooter();