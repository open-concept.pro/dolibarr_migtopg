<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
 *
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
*  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
*  \file       htdocs/custom/migtopg/exec/migtopg_step2.php
*  \ingroup    MigToPG
*  \brief      Second Step of the migration script
*/

$res=@include("../../main.inc.php");				// For root directory
if (! $res) $res=@include("../../../main.inc.php");	// For "custom" directory
if (! $res) die("Include of main fails");

dol_include_once('/migtopg/class/mysql_structures.class.php');

$langs->load('migtopg@migtopg');

if (!$user->rights->migtopg->execmig) accessforbidden();

$error = 0;
$mesg_err = array();
$sql_build_tables='';
$sql_build_constraints='';

//Contruct current table structure on Mysql
$mysql_structure = new MigToPG_MySQLStructure($db);
$result=$mysql_structure->getListTables(true,true,true);
if ($result>0) {
	$ret=$mysql_structure->createStructureScriptTables($conf->global->MIG_TO_PG_DB_USER,true,true,false);
	if ($ret>0) {
		$sql_build_tables=$mysql_structure->sql_build_tables;
	}
	$ret=$mysql_structure->createStructureScriptTables($conf->global->MIG_TO_PG_DB_USER,false,false,true);
	if ($ret>0) {
		$sql_build_constraints=$mysql_structure->sql_build_tables;
	}
}else {
	$mesg_err[]=$langs->trans("MigToPGStructureFileErr").':'.$mysql_structure->error;
	$error++;
}

$dedup_html='';
if (count($mysql_structure->de_dbl_constraint_name)>0) {
	$dedup_html.='<table width="100%" class="border">';
	$dedup_html.='<tr class="liste_titre">';
	$dedup_html.='<td>'.$langs->trans("MigToPGTable").'</td>';
	$dedup_html.='<td>'.$langs->trans("MigToPGOldName").'</td>';
	$dedup_html.='<td>'.$langs->trans("MigToPGNewName").'</td>';
	$dedup_html.='<td>'.$langs->trans("MigToPGType").'</td>';
	$dedup_html.="</tr>\n";
	
	$style='impair';
	foreach($mysql_structure->de_dbl_constraint_name as $unique_name) {
		if ($style=='pair') {$style='impair';}
		else {$style='pair';}
		$dedup_html.='<tr class="'.$style.'">';
		$dedup_html.='<td>'.$unique_name->table_name.'</td>';
		$dedup_html.='<td>'.$unique_name->old_name.'</td>';
		$dedup_html.='<td>'.$unique_name->new_name.'</td>';
		$dedup_html.='<td>'.$unique_name->type.'</td>';
		$dedup_html.="</tr>\n";
	}
	$dedup_html.='</table>';
}

if ((!$error) && (!empty($sql_build_tables))){
	
	// Definition of $dir and $file
	$dir = $conf->migtopg->dir_output;
	$file = $dir.'/'.'tbl_structure.sql';
	
	if (file_exists($dir))
	{
		if (file_exists($file)) {unlink($file);}
		$file=fopen($file,"a+");
		if ($file!==false) {
			fwrite($file,$sql_build_tables."\n");
			fclose($file);
		}else {
			$mesg_err[]=$langs->trans("MigToPGStructureFileErr").':'.$langs->trans("MigToPGSavStructureFileErr",$dir);
			$error++;
		}
	}
	
	// Definition of $dir and $file
	$dir = $conf->migtopg->dir_output;
	$file = $dir.'/'.'tbl_constaints.sql';
	
	if (file_exists($dir))
	{
		if (file_exists($file)) {unlink($file);}
		$file=fopen($file,"a+");
		if ($file!==false) {
			fwrite($file,$sql_build_constraints."\n");
			fclose($file);
		}else {
			$mesg_err[]=$langs->trans("MigToPGStructureFileErr").':'.$langs->trans("MigToPGSavStructureFileErr",$dir);
			$error++;
		}
		
	}
	
}else {
	$mesg_err[]=$langs->trans("MigToPGStructureFileErr").':'.$langs->trans("MigToPGGenStructureFileErr").':'.$mysql_structure->error;
	$error++;
}

/*
 *  Page
*
*/

llxHeader();

$form=new Form($db);

print_titre($langs->trans("MigToPGStructureFile"));

if ($error) {
	dol_htmloutput_errors('',$mesg_err);
}

print '<table width="100%" class="border">';
print '<tr class="liste_titre">';
print '<td width="20%">'.$langs->trans("MigToPGCheck").'</td>';
print '<td width="5%">'.$langs->trans("MigToPGResult").'</td>';
print '<td align="left">'.$langs->trans("MigToPGComment").'</td>';
print "</tr>\n";

//Fichier de creation de db
print '<tr class="pair"><td>'.$langs->trans("MigToPGStructureFile").'</td>';
print '<td align="left">';
if ((!$error) && (empty($dedup_html))) {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/tick.png',1).'" alt="Ok">';
}elseif (!empty($dedup_html)) {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/warning.png',1).'" alt="Ok">';
} else {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="Ko">';
}
print '</td><td align="left">';
if (!$error) {
	print $langs->trans("MigToPGStructureFile").':';
	print '<a href="'.dol_buildpath('/document.php',1).'?modulepart=migtopg&file=tbl_structure.sql" alt="'.$langs->trans("MigToPGStructureFile").'" title="'.$langs->trans("MigToPGStructureFile").'">';
	print '<img src="'.dol_buildpath('/migtopg/img/script_file.png',1).'" border="0" align="absmiddle" hspace="2px" ></a>';
	print '<BR>';
	print $langs->trans("MigToPGStructureFileConstraint").'('.$langs->trans("MigToPGStructureFileConstraintExecStep5").'):';
	print '<a href="'.dol_buildpath('/document.php',1).'?modulepart=migtopg&file=tbl_constaints.sql" alt="'.$langs->trans("MigToPGStructureFileConstraint").'" title="'.$langs->trans("MigToPGStructureFileConstraint").'">';
	print '<img src="'.dol_buildpath('/migtopg/img/script_file.png',1).'" border="0" align="absmiddle" hspace="2px" ></a>';
}else {
	dol_htmloutput_errors('',$mesg_err,1);
}
if (!empty($dedup_html) && (!$error)) {
	print '<BR>';
	print $langs->trans("MigToPGChangeIndexName").'<BR>';
	print $dedup_html;
}
print '</td>';
print '</tr>';

print '</table>';


// Boutons actions
print '<div class="tabsAction">';

if ($user->rights->migtopg->execmig && !$error)
{
	print '<a class="butAction" href="'.dol_buildpath('/migtopg/exec/migtopg_step3.php?action=execscript',1).'">'.$langs->trans("MigToPGExecuteStep3").'</a>';
}
else
{
	print '<a class="butActionRefused" href="#" title="'.$langs->trans("MigToPGExecuteStep3").'">'.$langs->trans("MigToPGExecuteStep3").'</a>';
}

print '</div>';

$db->close();

llxFooter();