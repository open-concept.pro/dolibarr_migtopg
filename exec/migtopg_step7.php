<?php
/* Copyright (C) 2012       Florian Henry  	<florian.henry@open-concept.pro>
 *
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 * 	\defgroup   MigrationToPostgreSQL     Module MigrationToPostgreSQL
 *  \brief      This module is build to migrate any version of Dolibarr from MySQL (>=5.4) to PostgreSQL (>=8.4)
 *  \file       htdocs/custom/migtopg/exec/migtopg_step7.php
*  \ingroup    MigToPG
*  \brief      7th Step of the migration script
*/

$res=@include("../../main.inc.php");				// For root directory
if (! $res) $res=@include("../../../main.inc.php");	// For "custom" directory
if (! $res) die("Include of main fails");

dol_include_once('/migtopg/class/migtopg.class.php');
dol_include_once('/migtopg/class/mysql_structures.class.php');

$langs->load('migtopg@migtopg');

if (!$user->rights->migtopg->execmig) accessforbidden();

$action = GETPOST('action','alpha');

$error = 0;
$mesg_err = array();

//Create own database object
$MigToPg_db = new MigToPG();

$MigToPg_db->pg_host=$conf->global->MIG_TO_PG_DB_PG_SERVER;
$MigToPg_db->pg_port=$conf->global->MIG_TO_PG_DB_PG_PORT;
$MigToPg_db->pg_user=$conf->global->MIG_TO_PG_DB_USER;
$MigToPg_db->pg_password=$conf->global->MIG_TO_PG_DB_PASS;
$MigToPg_db->pg_db_name=$conf->global->MIG_TO_PG_DB_NAME;
$MigToPg_db->pg_root_user=$conf->global->MIG_TO_PG_DB_RUSER;
$MigToPg_db->pg_root_password=$conf->global->MIG_TO_PG_DB_RPASS;

// Definition of $dir and $file
$dir = $conf->migtopg->dir_output;
$file_specialfield = $dir.'/'.'field_autoupdate.sql';

$ret=$MigToPg_db->connectPgSQL($MigToPg_db->pg_host, $MigToPg_db->pg_user, $MigToPg_db->pg_password, $MigToPg_db->pg_db_name, $MigToPg_db->pg_port);
if ($ret<0) {
	$mesg_err[] = $MigToPg_db->error;
	$error++;
}

if (!$error) {
	//Contruct current table structure on Mysql
	$mysql_structure = new MigToPG_MySQLStructure($db);
	$result=$mysql_structure->getListTables(true,false,false);
	if ($result>0) {
		//var_export($mysql_structure->mysql_tables);
		$ret=$mysql_structure->createScriptSpecialField();
		if ($ret>0) {
			$sql_field_autoupdate=$mysql_structure->sql_field_autoupdate;
		}
	}else {
		$mesg_err[]=$langs->trans("MigToPGSpecialFileErr").':'.$mysql_structure->error;
		$error++;
	}
} 

if ((!$error) && (!empty($sql_field_autoupdate))){

	if (file_exists($dir))
	{
		if (file_exists($file_specialfield)) {unlink($file_specialfield);}
		$file=fopen($file_specialfield,"a+");
		if ($file!==false) {
			fwrite($file,$sql_field_autoupdate."\n");
			fclose($file);
		}else {
			$mesg_err[]=$langs->trans("MigToPGSpecialFileErr").':'.$langs->trans("MigToPGSavStructureFileErr",$file_specialfield);
			$error++;
		}
	}
}

if ((!$error) && ($action=='execscript')) {
	
	if (file_exists($file_specialfield)) {
		$buffer = file_get_contents($file_specialfield);
		if ($buffer!==false) {

			$ret = @pg_query($MigToPg_db->db, $buffer);
			if (! $ret)	{
				$mesg_err[]=pg_last_error($MigToPg_db->db);
				$error++;
			}
			
		} else {
			$mesg_err[]=$langs->trans("MigToPGStructureFileOpenErr",$file_specialfield);
			$error++;
		}
	}else {
		$mesg_err[]=$langs->trans("MigToPGStructureFileOpenErr",$file_specialfield);
		$error++;
	}
}

/*
 *  Page
*
*/

llxHeader();

$form=new Form($db);

// Admin var of module
print_titre($langs->trans("MigToPGExecuteStep7"));

print '<table width="100%" class="border">';
print '<tr class="liste_titre">';
print '<td width="20%">'.$langs->trans("MigToPGCheck").'</td>';
print '<td width="5%">'.$langs->trans("MigToPGResult").'</td>';
print '<td align="left">'.$langs->trans("MigToPGComment").'</td>';
print "</tr>\n";

//Fichier de creation de db
print '<tr class="pair"><td>'.$langs->trans("MigToPGExecuteStep7").'</td>';
print '<td align="left">';
if ((!$error) && ($action=='execscript')) {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/tick.png',1).'" alt="Ok">';
} else {
	print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="Ko">';
}
print '</td><td align="left">';
if ((!$error) && ($action=='execscript')) {
	print $langs->trans("MigToPGExecuteSuccess").'<BR>';
}else {
	dol_htmloutput_errors('',$mesg_err,1);
}
print $langs->trans("MigToPGSpecialFileFunction").':';
print '<a href="'.dol_buildpath('/document.php',1).'?modulepart=migtopg&file=field_autoupdate.sql" alt="'.$langs->trans("MigToPGStructureFileFunction").'" title="'.$langs->trans("MigToPGStructureFileConstraint").'">';
print '<img src="'.dol_buildpath('/migtopg/img/script_file.png',1).'" border="0" align="absmiddle" hspace="2px" ></a>';
print '</td>';
print '</tr>';


//Rebuild sequence
print '<tr class="pair"><td>'.$langs->trans("MigToPGExecuteStep7RebuildSequence").'</td>';
print '<td align="left">';
if ($action=='execscript') {
	
	$ret = @pg_query($MigToPg_db->db, 'SELECT dol_util_rebuild_sequences();');
	if (! $ret)	{
		print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/error.png',1).'" alt="Ko">';
		$mesg_err[]=pg_last_error($MigToPg_db->db);
		$error++;
	}else {
		print '<img src="'.dol_buildpath('/theme/'.$conf->theme.'/img/tick.png',1).'" alt="Ok">';
		$error=0;
	}
}
print '</td><td align="left">';
if ((!$error) && ($action=='execscript')) {
	print $langs->trans("MigToPGExecuteSuccess").'<BR>';
}else {
	dol_htmloutput_errors('',$mesg_err,1);
}
print '</td>';
print '</tr>';

print '</table>';

// Boutons actions
print '<div class="tabsAction">';

if ($user->rights->migtopg->execmig && !$error)
{
	print '<a class="butAction" href="'.dol_buildpath('/migtopg/exec/migtopg_step8.php',1).'?action=check">'.$langs->trans("MigToPGExecuteStep8").'</a>';
}
else
{
	print '<a class="butActionRefused" href="#" title="'.$langs->trans("MigToPGExecuteStep8").'">'.$langs->trans("MigToPGExecuteStep8").'</a>';
}

print '</div>';

$db->close();

llxFooter();